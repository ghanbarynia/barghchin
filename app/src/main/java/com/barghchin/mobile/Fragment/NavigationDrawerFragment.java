package com.barghchin.mobile.Fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.barghchin.mobile.ModelData.ShoppingBagModel;
import com.barghchin.mobile.ModelData.UserProfileModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.activity.CategoriesActivity;
import com.barghchin.mobile.activity.CompleteListActivity;
import com.barghchin.mobile.activity.FavoritesActivity;
import com.barghchin.mobile.activity.LogInActivity;
import com.barghchin.mobile.activity.ShoppingHistoryActivity;
import com.barghchin.mobile.activity.WeblogActivity;
import com.barghchin.mobile.utility.MySharedPreferences;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.mApplication;
import com.google.gson.Gson;

import java.lang.reflect.Field;

public class NavigationDrawerFragment extends Fragment implements View.OnClickListener {

    private Activity mActivity;
    private DrawerLayout mDrawerLayout;
    private TextView mTV_Tv_CountBage;
    private View mLayout_ExitAccount, mLayout_LogIn;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        mTV_Tv_CountBage.setText(String.valueOf(ShoppingBagModel.fromString(MySharedPreferences.getInstance().getString(MySharedPreferences.SHOPPING_BAG, "")).getBags().size()));
        if (Utils.isLogin()) {
            mLayout_LogIn.setVisibility(View.GONE);
            mLayout_ExitAccount.setVisibility(View.VISIBLE);
        } else {
            mLayout_LogIn.setVisibility(View.VISIBLE);
            mLayout_ExitAccount.setVisibility(View.GONE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mDrawerRelativeLayout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        initializeControlls(mDrawerRelativeLayout);
        return mDrawerRelativeLayout;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.LogInActivity_Layout_Logo:
                startActivity(new Intent(mActivity, LogInActivity.class));
                break;
            case R.id.Layout_Menu_Categories:
                startActivity(new Intent(mActivity, CategoriesActivity.class));
                break;
            case R.id.Layout_Menu_PurchaseHistory:
                UserProfileModel profileModel = new Gson().fromJson(MySharedPreferences.getInstance().getString(MySharedPreferences.USER_PROFILE, ""), UserProfileModel.class);
                if (profileModel != null && profileModel.getId() > 0) {
                    startActivity(new Intent(mActivity, ShoppingHistoryActivity.class).putExtra("ID", profileModel.getId()));
                } else {
                    Utils.Toast("لطفا ابتدا وارد حساب کاربری شوید");
                }
                break;
            case R.id.Layout_Menu_ShoppingBag:
                Utils.startShoppingBagActivity(mActivity);
                break;
            case R.id.Layout_Menu_Favorites:
                startActivity(new Intent(mActivity, FavoritesActivity.class));
                break;
            case R.id.Layout_Menu_ExitAccount:
                MySharedPreferences.getInstance().RemovingSinglePreference(MySharedPreferences.USER_PROFILE);
                mLayout_LogIn.setVisibility(View.VISIBLE);
                mLayout_ExitAccount.setVisibility(View.GONE);
                break;
            case R.id.Layout_Menu_Weblog:
                startActivity(new Intent(mActivity, WeblogActivity.class));
                break;
            case R.id.Layout_Menu_LatestProducts:
                Intent intent1 = new Intent(mActivity, CompleteListActivity.class);
                intent1.putExtra("ModelTitle", getString(R.string.Menu_LatestProducts));
                intent1.putExtra("Model", CompleteListActivity.LatestProducts);
                startActivity(intent1);
                break;
            case R.id.Layout_Menu_Best_ellingProducts:
                Intent intent2 = new Intent(mActivity, CompleteListActivity.class);
                intent2.putExtra("ModelTitle", getString(R.string.Menu_Best_ellingProducts));
                intent2.putExtra("Model", CompleteListActivity.Best_ellingProducts);
                startActivity(intent2);
                break;
        }
        mDrawerLayout.closeDrawers();
    }

    private void initializeControlls(View view) {
        mActivity = getActivity();
        view.findViewById(android.R.id.content).setLayoutParams(new LinearLayout.LayoutParams((int) (mApplication.mPoint.x * 0.78), LinearLayout.LayoutParams.MATCH_PARENT));

        TextView mTV_MenuHome = (TextView) view.findViewById(R.id.TV_Menu_Home);
//        TextView mTV_MenuBookmarks = (TextView) view.findViewById(R.id.TV_Menu_Bookmarks);
        TextView mTV_MenuShoppingBag = (TextView) view.findViewById(R.id.TV_Menu_ShoppingBag);
        mTV_Tv_CountBage = (TextView) view.findViewById(R.id.Tv_CountBage);

        mTV_MenuHome.setTypeface(mApplication.IRANSans);
//        mTV_MenuBookmarks.setTypeface(mApplication.IRANSans);
        mTV_MenuShoppingBag.setTypeface(mApplication.IRANSans);
        mTV_Tv_CountBage.setTypeface(mApplication.IRANSans);

        view.findViewById(R.id.Layout_Menu_LatestProducts).setOnClickListener(this);
        view.findViewById(R.id.Layout_Menu_Best_ellingProducts).setOnClickListener(this);
//        view.findViewById(R.id.Layout_Menu_Home).setOnClickListener(this);
        view.findViewById(R.id.Layout_Menu_Categories).setOnClickListener(this);
//        view.findViewById(R.id.Layout_Menu_Bookmarks).setOnClickListener(this);
        view.findViewById(R.id.Layout_Menu_ShoppingBag).setOnClickListener(this);
        view.findViewById(R.id.Layout_Menu_Favorites).setOnClickListener(this);
        view.findViewById(R.id.Layout_Menu_PurchaseHistory).setOnClickListener(this);
        mLayout_ExitAccount = view.findViewById(R.id.Layout_Menu_ExitAccount);
        mLayout_ExitAccount.setOnClickListener(this);
        mLayout_LogIn = view.findViewById(R.id.LogInActivity_Layout_Logo);
        mLayout_LogIn.setOnClickListener(this);
        view.findViewById(R.id.Layout_Menu_Weblog).setOnClickListener(this);
//        view.findViewById(R.id.Layout_Menu_MostViewedProducts).setOnClickListener(this);
//        view.findViewById(R.id.Layout_Menu_LatestProducts).setOnClickListener(this);
    }


    public void setUp(DrawerLayout drawerLayout) {
        mDrawerLayout = drawerLayout;
        getOverflowMenu();
    }

    private void getOverflowMenu() {
        try {
            ViewConfiguration config = ViewConfiguration.get(getActivity().getApplicationContext());
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
