package com.barghchin.mobile.dataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.barghchin.mobile.ModelData.ModuleDataModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ghanbarinia on 9/23/2017.
 */

public class dbHandler {

    private static final String DATABASE_NAME = "dataBase.db";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_PRODUCT = "product";
    public static final String KEY_PRODUCT_ID = BaseColumns._ID;
    public static final String KEY_PRODUCT_TITLE = "title";
    public static final String KEY_PRODUCT_SHORT_DESCRIPTION = "short_description";
    public static final String KEY_PRODUCT_IMAGE = "image";
    private final SQLiteDatabase db;
    private static dbHandler instance;

    public dbHandler(Context context) {
        SQLiteHelper mHelper = new SQLiteHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
        db = mHelper.getWritableDatabase();
    }

    public static synchronized dbHandler getInstance(Context context) {
        if (instance == null) {
            instance = new dbHandler(context);
        }
        return instance;
    }

    public boolean isFavorite(int id) {
        Cursor c = db.rawQuery("SELECT Count(*)  FROM " + TABLE_PRODUCT + "  where " + KEY_PRODUCT_ID + " = " + id, null);
        c.moveToFirst();
        int count = c.getInt(0);
        c.close();
        return count == 1;

    }

    public void insertProduct(ContentValues values) {
        db.insert(TABLE_PRODUCT, null, values);
    }

    public void deleteProduct(String ID) {
        db.delete(TABLE_PRODUCT, KEY_PRODUCT_ID + " = ?", new String[]{String.valueOf(ID)});
    }

    public List<ModuleDataModel> selectGalleryPack() {
        Cursor c = db.rawQuery("SELECT  * FROM " + TABLE_PRODUCT, null);
        List<ModuleDataModel> lst = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                ModuleDataModel model = new ModuleDataModel();
                model.setId(c.getInt(c.getColumnIndexOrThrow(KEY_PRODUCT_ID)));
                model.setTitle(c.getString(c.getColumnIndexOrThrow(KEY_PRODUCT_TITLE)));
                model.setShort_description(c.getString(c.getColumnIndexOrThrow(KEY_PRODUCT_SHORT_DESCRIPTION)));
                model.setImage(c.getString(c.getColumnIndexOrThrow(KEY_PRODUCT_IMAGE)));
                lst.add(model);
            } while (c.moveToNext());
        }
        c.close();
        return lst;
    }
}
