package com.barghchin.mobile.dataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Ghanbarinia on 9/23/2017.
 */

public class SQLiteHelper extends SQLiteOpenHelper {


    public SQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_PRODUCT_TABLE = String.format("CREATE TABLE IF NOT EXISTS %s (%s INTEGER PRIMARY KEY, %s TEXT, %s TEXT, %s TEXT)",
                dbHandler.TABLE_PRODUCT, dbHandler.KEY_PRODUCT_ID, dbHandler.KEY_PRODUCT_TITLE, dbHandler.KEY_PRODUCT_SHORT_DESCRIPTION, dbHandler.KEY_PRODUCT_IMAGE);

        db.execSQL(CREATE_PRODUCT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < newVersion) {
            db.execSQL("ALTER TABLE " + dbHandler.TABLE_PRODUCT);
        }
    }
}
