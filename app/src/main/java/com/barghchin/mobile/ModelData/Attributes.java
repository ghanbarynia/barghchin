package com.barghchin.mobile.ModelData;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ghanbarinia on 8/12/2017.
 */

public class Attributes implements Serializable {
    private String name,option;
    private List<String> options = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getOptions() {
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }
}
