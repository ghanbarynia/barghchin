package com.barghchin.mobile.ModelData;

import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ghanbarinia on 8/15/2017.
 */

public class ShoppingBagModel implements Serializable {
    private List<ShoppingBag> bags=new ArrayList<>();

    public List<ShoppingBag> getBags() {
        return bags;
    }

    public void setBags(List<ShoppingBag> bags) {
        this.bags = bags;
    }

    public static ShoppingBagModel fromString(String array) {
        ShoppingBagModel model = new Gson().fromJson(array, ShoppingBagModel.class);
        if (model == null) {
            return new ShoppingBagModel();
        }
        return model;
    }

    public String toString() {
        return new Gson().toJson(this);
    }
}
