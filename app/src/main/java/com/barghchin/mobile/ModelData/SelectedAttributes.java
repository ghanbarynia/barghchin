package com.barghchin.mobile.ModelData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Ghanbarinia on 8/15/2017.
 */

class SelectedAttributes implements Serializable {
    private int id, option;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOption() {
        return option;
    }

    public void setOption(int option) {
        this.option = option;
    }

    public String toString() {
        try {
        JSONObject jsonObject= new JSONObject();
            jsonObject.put("id", getId());
            jsonObject.put("option", getOption());
            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }
}
