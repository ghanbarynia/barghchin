package com.barghchin.mobile.ModelData;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class MainActivityModel implements Serializable {

    private String bgColor, textColor, title, linkId, linkId2, imageUrl, imageUrl2;
    private int type, itemsCount, likeType, likeType2;
    private List<ModuleDataModel> module_data = new ArrayList<>();

    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLinkId() {
        return linkId;
    }

    public void setLinkId(String linkId) {
        this.linkId = linkId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getItemsCount() {
        return itemsCount;
    }

    public void setItemsCount(int itemsCount) {
        this.itemsCount = itemsCount;
    }

    public int getLikeType() {
        return likeType;
    }

    public void setLikeType(int likeType) {
        this.likeType = likeType;
    }

    public List<ModuleDataModel> getModule_data() {
        return module_data;
    }

    public void setModule_data(List<ModuleDataModel> module_data) {
        this.module_data = module_data;
    }

    public String getLinkId2() {
        return linkId2;
    }

    public void setLinkId2(String linkId2) {
        this.linkId2 = linkId2;
    }

    public String getImageUrl2() {
        return imageUrl2;
    }

    public void setImageUrl2(String imageUrl2) {
        this.imageUrl2 = imageUrl2;
    }

    public int getLikeType2() {
        return likeType2;
    }

    public void setLikeType2(int likeType2) {
        this.likeType2 = likeType2;
    }
}
