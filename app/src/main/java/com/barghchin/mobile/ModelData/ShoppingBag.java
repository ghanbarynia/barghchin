package com.barghchin.mobile.ModelData;

import java.io.Serializable;

/**
 * Created by Ghanbarinia on 8/13/2017.
 */

public class ShoppingBag implements Serializable {
    private String image, title, price, regular_price;
    private int id, count = 1;
    private boolean on_sale;
    private ModuleDataModel attribute;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRegular_price() {
        return regular_price;
    }

    public void setRegular_price(String regular_price) {
        this.regular_price = regular_price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ModuleDataModel getAttribute() {
        return attribute;
    }

    public void setAttribute(ModuleDataModel attribute) {
        this.attribute = attribute;
    }

    public boolean isOn_sale() {
        return on_sale;
    }

    public void setOn_sale(boolean on_sale) {
        this.on_sale = on_sale;
    }
}
