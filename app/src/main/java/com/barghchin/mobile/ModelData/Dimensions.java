package com.barghchin.mobile.ModelData;

import java.io.Serializable;

/**
 * Created by Ghanbarinia on 8/12/2017.
 */

public class Dimensions implements Serializable {
    private String length, width, height;

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }
}
