package com.barghchin.mobile.Interface;


import com.barghchin.mobile.ModelData.VolleyResponseModel;

public interface BaseCallBackService {

    void success(VolleyResponseModel callback);

    void error(VolleyResponseModel callback);
}
