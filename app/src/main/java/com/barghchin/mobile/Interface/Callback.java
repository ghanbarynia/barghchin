package com.barghchin.mobile.Interface;

public interface Callback {
  void onSuccess();

  void onError();

  public static class EmptyCallback implements Callback {

    @Override public void onSuccess() {
    }

    @Override public void onError() {
    }
  }
}
