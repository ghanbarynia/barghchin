package com.barghchin.mobile.Interface;

import android.view.View;

public interface OnItemClickListener {
    void onClick(View view,int position);
}
