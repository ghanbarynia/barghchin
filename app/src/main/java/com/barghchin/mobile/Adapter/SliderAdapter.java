package com.barghchin.mobile.Adapter;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.barghchin.mobile.Interface.OnItemClickListener;
import com.barghchin.mobile.utility.Constant;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.mApplication;

import java.util.List;

public class SliderAdapter extends PagerAdapter {
    private Activity mActivity;
    private List<String> mData;
    private OnItemClickListener mListener;

    public SliderAdapter(Activity activity, List<String> mainActivityModel, OnItemClickListener listener) {
        mActivity = activity;
        mData = mainActivityModel;
        mListener = listener;
    }

    @Override
    public int getCount() {
        if (mData == null) {
            return 0;
        }
        return mData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup view, int position, Object object) {
        view.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        ImageView imageView = new ImageView(mActivity);
        Utils.loadImage(imageView,mData.get(position));
        imageView.setTag(position);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onClick(v, position);
                }
            }
        });
        view.addView(imageView, mApplication.mPoint.x, (int) (mApplication.mPoint.x * Constant.AspectRatio));
        return imageView;
    }
}