package com.barghchin.mobile.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.barghchin.mobile.ModelData.OrdersModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.utility.mApplication;

import java.util.List;

/**
 * Created by Ghanbarinia on 7/18/2017.
 */

public class OrdersAdapter extends ArrayAdapter<OrdersModel> {

    private LayoutInflater mInflater;
    private int mResource;

    public OrdersAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<OrdersModel> objects) {
        super(context, resource, objects);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(mResource, parent, false);
            holder = new ViewHolder();
            holder.mTitle = (TextView) convertView.findViewById(R.id.TV_Title);
            holder.mTitle.setTypeface(mApplication.IRANSans, Typeface.BOLD);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        OrdersModel model = getItem(position);
        String txt = "سفارش شماره  : " + model.getId() + " " + model.getStatus();
        holder.mTitle.setText(txt);
        convertView.setTag(txt);
        return convertView;
    }

    private class ViewHolder {
        TextView mTitle;
    }
}
