package com.barghchin.mobile.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.barghchin.mobile.Interface.Callback;
import com.barghchin.mobile.ModelData.WeblogModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.activity.WeblogDetailsActivity;
import com.barghchin.mobile.utility.PersianCalendar;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.mApplication;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Ghanbarinia on 10/3/2017.
 */

public class CustomAdapter_Weblog extends RecyclerView.Adapter<CustomAdapter_Weblog.ViewHolder> {

    private Activity mActivity;
    private List<WeblogModel> mDataModel;

    public CustomAdapter_Weblog(Activity act, List<WeblogModel> data) {
        this.mActivity = act;
        this.mDataModel = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_weblog, parent, false);
        return new CustomAdapter_Weblog.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mTitle.setText(Utils.fromHtml(mDataModel.get(position).getTitle()));
        holder.mShortDescription.setText(Utils.fromHtml(mDataModel.get(position).getContent()));

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
        Date date = null;
        try {
            date = sdf.parse(mDataModel.get(position).getDate());
        } catch (Exception e) {
            e.printStackTrace();
        }
        PersianCalendar persianCalendar = new PersianCalendar();

        holder.mDate.setText(persianCalendar.getPersianDate(date));
        holder.mProgressBar.setVisibility(View.VISIBLE);
        Utils.loadImage(holder.mImage, mDataModel.get(position).getImage(), R.drawable.place_holder, new Callback() {
            @Override
            public void onSuccess() {
                holder.mProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                holder.mProgressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mDataModel == null) {
            return 0;
        }
        return mDataModel.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView mImage;
        private TextView mTitle, mShortDescription, mDate;
        private ProgressBar mProgressBar;

        ViewHolder(View view) {
            super(view);
            assignViews();
            view.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            mProgressBar.getIndeterminateDrawable().setColorFilter(Utils.getAppThemeColor(), PorterDuff.Mode.MULTIPLY);

            final int size = mApplication.mPoint.x / 4;
            mImage.setLayoutParams(new FrameLayout.LayoutParams(size, size));

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mActivity.startActivity(new Intent(mActivity, WeblogDetailsActivity.class).putExtra("MODEL", mDataModel.get(getAdapterPosition())));
                }
            });
        }

        private View findViewById(final int id) {
            return itemView.findViewById(id);
        }

        private void assignViews() {
            mImage = (ImageView) findViewById(R.id.image);
            mTitle = (TextView) findViewById(R.id.title);
            mDate = (TextView) findViewById(R.id.date);
            mShortDescription = (TextView) findViewById(R.id.short_description);
            mProgressBar = (ProgressBar) findViewById(R.id.Pb_Center);
        }
    }
}
