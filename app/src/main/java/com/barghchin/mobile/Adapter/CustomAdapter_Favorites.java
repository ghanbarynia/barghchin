package com.barghchin.mobile.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.barghchin.mobile.Interface.Callback;
import com.barghchin.mobile.ModelData.ModuleDataModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.activity.ProductDetailsActivity;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.mApplication;

import java.util.List;


public class CustomAdapter_Favorites extends RecyclerView.Adapter<CustomAdapter_Favorites.ViewHolder> {

    private Activity mActivity;
    private List<ModuleDataModel> mDataModel;

    public CustomAdapter_Favorites(Activity act, List<ModuleDataModel> data) {
        this.mActivity = act;
        this.mDataModel = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_favorites, parent, false);
        return new CustomAdapter_Favorites.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mTitle.setText(mDataModel.get(position).getTitle());
        holder.mShortDescription.setText(mDataModel.get(position).getShort_description());
        holder.mProgressBar.setVisibility(View.VISIBLE);
        Utils.loadImage(holder.mImage, mDataModel.get(position).getImage(), R.drawable.place_holder, new Callback() {
            @Override
            public void onSuccess() {
                holder.mProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                holder.mProgressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mDataModel == null) {
            return 0;
        }
        return mDataModel.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView mImage;
        private TextView mTitle, mShortDescription, mTvDelete;
        private ProgressBar mProgressBar;

        ViewHolder(View view) {
            super(view);
            assignViews();
            view.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            mProgressBar.getIndeterminateDrawable().setColorFilter(Utils.getAppThemeColor(), PorterDuff.Mode.MULTIPLY);

            final int size = mApplication.mPoint.x / 4;
            mImage.setLayoutParams(new FrameLayout.LayoutParams(size, size));

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mActivity.startActivity(new Intent(mActivity, ProductDetailsActivity.class).putExtra("ID", mDataModel.get(getAdapterPosition()).getId()));
                }
            });

            mTvDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mApplication.db.deleteProduct(String.valueOf(mDataModel.get(getAdapterPosition()).getId()));
                    mDataModel.remove(getAdapterPosition());
                    notifyDataSetChanged();
                }
            });
        }

        private View findViewById(final int id) {
            return itemView.findViewById(id);
        }

        private void assignViews() {
            mImage = (ImageView) findViewById(R.id.image);
            mTitle = (TextView) findViewById(R.id.title);
            mShortDescription = (TextView) findViewById(R.id.short_description);
            mProgressBar = (ProgressBar) findViewById(R.id.Pb_Center);
            mTvDelete = (TextView) findViewById(R.id.Tv_Delete);
        }
    }
}
