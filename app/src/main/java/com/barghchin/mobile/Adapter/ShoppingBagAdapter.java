package com.barghchin.mobile.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.barghchin.mobile.ModelData.ShoppingBag;
import com.barghchin.mobile.R;
import com.barghchin.mobile.activity.ProductDetailsActivity;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.mApplication;

import java.util.List;

/**
 * Created by Ghanbarinia on 8/16/2017.
 */

public class ShoppingBagAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<ShoppingBag> mShoppingBags;
    private Activity mActivity;

    public ShoppingBagAdapter(@NonNull Activity activity, @NonNull List<ShoppingBag> objects) {
        mActivity = activity;
        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mShoppingBags = objects;
    }

    @Override
    public int getCount() {
        if (mShoppingBags == null) {
            return 0;
        }
        return mShoppingBags.size();
    }

    @Override
    public ShoppingBag getItem(int position) {
        return mShoppingBags.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mShoppingBags.get(position).hashCode();
    }

    @SuppressLint("RtlHardcoded")
    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_shopping_bag, parent, false);
            holder = new ViewHolder();
            holder.mTitle = (TextView) convertView.findViewById(R.id.text_title);
            holder.mTextTotalPrice = (TextView) convertView.findViewById(R.id.text_total_price);
            holder.mTextFinalPrice = (TextView) convertView.findViewById(R.id.text_final_price);
            holder.mTextDelete = (TextView) convertView.findViewById(R.id.text_delete);
            holder.mBtnNegative = (ImageView) convertView.findViewById(R.id.btn_negative);
            holder.mBtnPositive = (ImageView) convertView.findViewById(R.id.btn_add_positive);
            holder.mImageProduct = (ImageView) convertView.findViewById(R.id.image_product);
            holder.mEtCount = (EditText) convertView.findViewById(R.id.et_count);
            holder.mLayoutContent = convertView.findViewById(R.id.layout_content);
            holder.mLayoutAttributes = (LinearLayout) convertView.findViewById(R.id.Layout_attributes);

            int size = (int) (mApplication.mPoint.x * 0.4);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(size, size);
            params.gravity = Gravity.CENTER_VERTICAL | Gravity.RIGHT;
            holder.mImageProduct.setLayoutParams(params);

            int size2 = (int) (mApplication.mPoint.x * 0.6);
            LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(size2, size2);
            params2.gravity = Gravity.CENTER_VERTICAL | Gravity.LEFT;
            holder.mLayoutContent.setLayoutParams(params2);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.mLayoutAttributes.removeAllViews();
        if (getItem(position).getAttribute() == null) {
            holder.mLayoutAttributes.setVisibility(View.GONE);
        } else {
            holder.mLayoutAttributes.setVisibility(View.VISIBLE);
            for (int i = 0; i < getItem(position).getAttribute().getAttributes().size(); i++) {
                View view = LayoutInflater.from(mActivity).inflate(R.layout.row_attributes, holder.mLayoutAttributes, false);
                TextView mTextName = (TextView) view.findViewById(R.id.text_name);
                TextView mTextOption = (TextView) view.findViewById(R.id.text_option);
                mTextName.setText(getItem(position).getAttribute().getAttributes().get(i).getName());
                mTextOption.setText(getItem(position).getAttribute().getAttributes().get(i).getOption());
                holder.mLayoutAttributes.addView(view);
            }
        }

        holder.mTitle.setText(getItem(position).getTitle());
        Utils.loadImage(holder.mImageProduct, getItem(position).getImage(), R.drawable.place_holder);
        holder.mEtCount.setText(String.valueOf(getItem(position).getCount()));
        holder.mTextTotalPrice.setText(getItem(position).getPrice());
        holder.mTextFinalPrice.setText(String.valueOf(Integer.parseInt(getItem(position).getPrice()) * getItem(position).getCount()));

        holder.mTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.startActivity(new Intent(mActivity, ProductDetailsActivity.class).putExtra("ID", mShoppingBags.get(position).getId()));
            }
        });

        holder.mImageProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.startActivity(new Intent(mActivity, ProductDetailsActivity.class).putExtra("ID", mShoppingBags.get(position).getId()));
            }
        });

        holder.mTextDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String array = MySharedPreferences.getInstance().getString(MySharedPreferences.SHOPPING_BAG, "");
//                ShoppingBagModel bagModel = ShoppingBagModel.fromString(array);
//                bagModel.getBags().remove(position);
//                MySharedPreferences.getInstance().setPreference(MySharedPreferences.SHOPPING_BAG, bagModel.toString());
                mShoppingBags.remove(position);
                notifyDataSetChanged();
            }
        });

        holder.mBtnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int n = mShoppingBags.get(position).getCount();
                n++;
                mShoppingBags.get(position).setCount(n);
                holder.mEtCount.setText(String.valueOf(n));

            }
        });

        holder.mBtnNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int n = mShoppingBags.get(position).getCount();
                if (n > 0) {
                    n--;
                    mShoppingBags.get(position).setCount(n);
                    holder.mEtCount.setText(String.valueOf(n));
                }

            }
        });

        holder.mEtCount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(s) && Integer.valueOf(s.toString()) >= 0 && position < mShoppingBags.size()) {
                    mShoppingBags.get(position).setCount(Integer.valueOf(s.toString()));
                    notifyDataSetChanged();
                }
            }
        });

        return convertView;
    }

    private class ViewHolder {
        TextView mTitle, mTextTotalPrice, mTextFinalPrice, mTextDelete;
        ImageView mBtnNegative, mBtnPositive, mImageProduct;
        EditText mEtCount;
        View mLayoutContent;
        LinearLayout mLayoutAttributes;
    }
}
