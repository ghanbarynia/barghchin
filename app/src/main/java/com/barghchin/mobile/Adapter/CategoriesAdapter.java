package com.barghchin.mobile.Adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.barghchin.mobile.ModelData.CategoriesModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.utility.Utils;

import java.util.List;

/**
 * Created by Ghanbarinia on 7/18/2017.
 */

public class CategoriesAdapter extends ArrayAdapter<CategoriesModel> {

    private LayoutInflater mInflater;
    private int mResource;

    public CategoriesAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<CategoriesModel> objects) {
        super(context, resource, objects);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(mResource, parent, false);
            holder = new ViewHolder();
            holder.mTitle = (TextView) convertView.findViewById(R.id.TV_Title);
            holder.mIcon = (ImageView) convertView.findViewById(R.id.Iv_Icon);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        CategoriesModel model = getItem(position);
        if (model != null) {
            holder.mTitle.setText(model.getName());
        }
        if (model != null) {
            Utils.loadImage(holder.mIcon,model.getImage(),R.drawable.place_holder);
        }
        return convertView;
    }

    private class ViewHolder {
        TextView mTitle;
        ImageView mIcon;
    }
}
