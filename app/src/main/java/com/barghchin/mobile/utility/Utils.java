package com.barghchin.mobile.utility;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatDrawableManager;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.barghchin.mobile.Interface.Callback;
import com.barghchin.mobile.ModelData.Attributes;
import com.barghchin.mobile.ModelData.Dimensions;
import com.barghchin.mobile.ModelData.ModuleDataModel;
import com.barghchin.mobile.ModelData.ShoppingBag;
import com.barghchin.mobile.ModelData.ShoppingBagModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.activity.LogInActivity;
import com.barghchin.mobile.activity.SearchActivity;
import com.barghchin.mobile.activity.ShoppingBagActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class Utils {
    public static void Toast(String text) {
        Toast(text, Toast.LENGTH_LONG);
    }

    public static void Toast(int text) {
        Toast(mApplication.applicationContext.getString(text), Toast.LENGTH_LONG);
    }

    public static void Toast(final String text, final int duration) {
        if (isEmpty(text)) {
            return;
        }
        if (Looper.myLooper() == Looper.getMainLooper()) {
            Toast.makeText(mApplication.applicationContext, text, duration).show();
        } else {
            mApplication.applicationHandler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(mApplication.applicationContext, text, duration).show();
                }
            });
        }
    }

    public static boolean checkInternetConnection(boolean toast) {
        Context context = mApplication.applicationContext;
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivity.getActiveNetworkInfo();
        boolean isConnected = netInfo != null && netInfo.isConnectedOrConnecting() && netInfo.isAvailable();
        if (!isConnected && toast) {
            Toast(context.getString(R.string.no_connection));
        }
        return isConnected;
    }

    public static String nullStrToEmpty(Object str) {
        return (str == null ? "" : (str instanceof String ? (String) str : str.toString()));
    }

    public static MaterialDialog.Builder CustomDialog(@NonNull Context context) {
        return new MaterialDialog.Builder(context)
                .titleGravity(GravityEnum.START)
                .contentGravity(GravityEnum.START)
                .btnStackedGravity(GravityEnum.END)
                .itemsGravity(GravityEnum.END)
                .buttonsGravity(GravityEnum.START)
                .typeface(mApplication.IRANSans, mApplication.IRANSans);
    }

    public static boolean isEmpty(String str) {
        return (str == null || str.trim().equals("null") || str.trim().length() <= 0);
    }

    public static int getColor(int id) {
        return ContextCompat.getColor(mApplication.applicationContext, id);
    }

    public static Drawable getDrawable(int id) {
        return AppCompatDrawableManager.get().getDrawable(mApplication.applicationContext, id);
    }

    @SuppressWarnings("ResourceType")
    public static int getAppThemeColor() {
        String color = MySharedPreferences.getInstance().getString(MySharedPreferences.STORE_COLOR, mApplication.applicationContext.getString(R.color.colorPrimary));
        return Color.parseColor(color);
    }

    @SuppressWarnings("deprecation")
    public static ModuleDataModel serializeJsonProduct(JSONObject jsonObject) throws JSONException {
        ModuleDataModel model = new ModuleDataModel();

        if (jsonObject.has("id") && !jsonObject.isNull("id")) {
            model.setId(jsonObject.getInt("id"));
        }

        if (jsonObject.has("name") && !jsonObject.isNull("name")) {
            model.setName(jsonObject.getString("name"));
        }

        if (jsonObject.has("type") && !jsonObject.isNull("type")) {
            model.setType(jsonObject.getString("type"));
        }

        if (jsonObject.has("status") && !jsonObject.isNull("status")) {
            model.setStatus(jsonObject.getString("status"));
        }

        if (jsonObject.has("featured") && !jsonObject.isNull("featured")) {
            model.setFeatured(jsonObject.getBoolean("featured"));
        }

        if (jsonObject.has("description") && !jsonObject.isNull("description")) {
            model.setDescription(fromHtml(jsonObject.getString("description")));
        }

        if (jsonObject.has("short_description") && !jsonObject.isNull("short_description")) {
            model.setShort_description(fromHtml(jsonObject.getString("short_description")));
        }

        if (jsonObject.has("price") && !jsonObject.isNull("price")) {
            model.setPrice(jsonObject.getString("price"));
        }

        if (jsonObject.has("regular_price") && !jsonObject.isNull("regular_price")) {
            model.setRegular_price(jsonObject.getString("regular_price"));
        }

        if (jsonObject.has("on_sale") && !jsonObject.isNull("on_sale")) {
            model.setOn_sale(jsonObject.getBoolean("on_sale"));
        }

        if (jsonObject.has("sale_price") && !jsonObject.isNull("sale_price")) {
            model.setSale_price(jsonObject.getString("sale_price"));
        }

        if (jsonObject.has("date_on_sale_to") && !jsonObject.isNull("date_on_sale_to")) {
            model.setDate_on_sale_to(jsonObject.getString("date_on_sale_to"));
        }

        if (jsonObject.has("in_stock") && !jsonObject.isNull("in_stock")) {
            model.setIn_stock(jsonObject.getBoolean("in_stock"));
        }

        if (jsonObject.has("average_rating") && !jsonObject.isNull("average_rating")) {
            model.setAverage_rating(jsonObject.getString("average_rating"));
        }

        if (jsonObject.has("images") && !jsonObject.isNull("images")) {
            List<String> images = new ArrayList<>();
            JSONArray arrayimages = jsonObject.getJSONArray("images");
            for (int i = 0; i < arrayimages.length(); i++) {
                JSONObject object = arrayimages.getJSONObject(i);
                if (object.has("src") && !object.isNull("src")) {
                    images.add(object.getString("src"));
                }
            }
            model.setImages(images);
        }

        if (jsonObject.has("reviews_allowed") && !jsonObject.isNull("reviews_allowed")) {
            model.setReviews_allowed(jsonObject.getBoolean("reviews_allowed"));
        }

        if (jsonObject.has("permalink") && !jsonObject.isNull("permalink")) {
            model.setPermalink(jsonObject.getString("permalink"));
        }

        if (jsonObject.has("rating_count") && !jsonObject.isNull("rating_count")) {
            model.setRating_count(jsonObject.getInt("rating_count"));
        }

        if (jsonObject.has("sku") && !jsonObject.isNull("sku")) {
            model.setSku(jsonObject.getString("sku"));
        }

        if (jsonObject.has("total_sales") && !jsonObject.isNull("total_sales")) {
            model.setTotal_sales(jsonObject.getInt("total_sales"));
        }

        if (jsonObject.has("weight") && !jsonObject.isNull("weight")) {
            model.setWeight(jsonObject.getString("weight"));
        }

        if (jsonObject.has("dimensions") && !jsonObject.isNull("dimensions")) {
            Dimensions dimensions = new Dimensions();
            JSONObject object = jsonObject.getJSONObject("dimensions");
            if (object.has("length") && !object.isNull("length")) {
                dimensions.setLength(object.getString("length"));
            }
            if (object.has("width") && !object.isNull("width")) {
                dimensions.setWidth(object.getString("width"));
            }
            if (object.has("height") && !object.isNull("height")) {
                dimensions.setHeight(object.getString("height"));
            }
            model.setDimensions(dimensions);
        }

        if (jsonObject.has("attributes") && !jsonObject.isNull("attributes")) {
            List<Attributes> attributes = new ArrayList<>();
            JSONArray array = jsonObject.getJSONArray("attributes");
            for (int i = 0; i < array.length(); i++) {
                Attributes attribute = new Attributes();
                JSONObject jsonObject1 = array.getJSONObject(i);
                if (jsonObject1.has("name") && !jsonObject1.isNull("name")) {
                    attribute.setName(jsonObject1.getString("name"));
                }
                if (jsonObject1.has("option") && !jsonObject1.isNull("option")) {
                    attribute.setOption(jsonObject1.getString("option"));
                }
                if (jsonObject1.has("options") && !jsonObject1.isNull("options")) {
                    List<String> options = new ArrayList<>();
                    JSONArray optionsArray = jsonObject1.getJSONArray("options");
                    for (int j = 0; j < optionsArray.length(); j++) {
                        options.add(optionsArray.getString(j));
                    }
                    attribute.setOptions(options);
                }

                attributes.add(attribute);
            }
            model.setAttributes(attributes);
        }

        if (jsonObject.has("external_url") && !jsonObject.isNull("external_url")) {
            model.setExternal_url(jsonObject.getString("external_url"));
        }

        if (jsonObject.has("button_text") && !jsonObject.isNull("button_text")) {
            model.setButton_text(jsonObject.getString("button_text"));
        }

        return model;
    }

    public static boolean isLogin() {
        return !TextUtils.isEmpty(MySharedPreferences.getInstance().getString(MySharedPreferences.USER_PROFILE, ""));
    }

    @SuppressLint("InflateParams")
    public static void dialogShoppingBag(Context mContext, final ShoppingBag model) {
        if (!isLogin() && !MySharedPreferences.getInstance().getBoolean(MySharedPreferences.PermissionPurchaseWithoutLogin, false)) {
            Toast("جهت ثبت محصول ، لطفا ابتدا ورود به سیستم کنید.");
            mContext.startActivity(new Intent(mContext, LogInActivity.class));
            return;
        }
        View view = LayoutInflater.from(mContext).inflate(R.layout.shopping_bag, null, false);
        ImageView image = (ImageView) view.findViewById(R.id.image);
        TextView title = (TextView) view.findViewById(R.id.title);
        TextView price = (TextView) view.findViewById(R.id.price);
        final EditText et_count = (EditText) view.findViewById(R.id.et_count);

        loadImage(image, model.getImage(), R.drawable.place_holder);
        title.setText(model.getTitle());
        price.setText(model.getPrice().concat(" تومان"));
        view.findViewById(R.id.btn_negative).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int n = Integer.parseInt(et_count.getText().toString());
                if (n > 1) {
                    n--;
                    et_count.setText(String.valueOf(n));
                    model.setCount(n);
                }
            }
        });
        view.findViewById(R.id.btn_add_positive).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int n = Integer.parseInt(et_count.getText().toString());
                n++;
                et_count.setText(String.valueOf(n));
                model.setCount(n);
            }
        });

        CustomDialog(mContext)
                .title("افزودن به سبد خرید")
                .positiveText("افزودن به سبد خرید")
                .negativeText("انصراف")
                .customView(view, true)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        String array = MySharedPreferences.getInstance().getString(MySharedPreferences.SHOPPING_BAG, "");
                        ShoppingBagModel bagModel = ShoppingBagModel.fromString(array);
                        int Id_temp = -1;
                        for (int i = 0; i < bagModel.getBags().size(); i++) {
                            ShoppingBag bag = bagModel.getBags().get(i);
                            if (bag.getId() == model.getId()) {
                                Id_temp = i;
                                break;
                            }
                        }
                        if (Id_temp == -1) {
                            bagModel.getBags().add(model);
                            Utils.Toast("محصول به سبد خرید اضافه شد");
                        } else {
                            bagModel.getBags().set(Id_temp, model);
                            Utils.Toast("محصول در سبد خرید بروز شد");
                        }
                        MySharedPreferences.getInstance().setPreference(MySharedPreferences.SHOPPING_BAG, bagModel.toString());
                    }
                }).show();
    }

    public static int dp(int value) {
        if (value == 0) {
            return 0;
        }
        return (int) Math.ceil(mApplication.applicationContext.getResources().getDisplayMetrics().density * value);
    }

    public static void startShoppingBagActivity(Context packageContext) {
        if (ShoppingBagModel.fromString(MySharedPreferences.getInstance().getString(MySharedPreferences.SHOPPING_BAG, "")).getBags().size() > 0) {
            packageContext.startActivity(new Intent(packageContext, ShoppingBagActivity.class));
        }
    }

    public static void startSearchActivity(Context packageContext) {
        packageContext.startActivity(new Intent(packageContext, SearchActivity.class));
    }

    public static String detectStatus(String status) {
        String ret = "";
        switch (status) {
            case "pending":
                ret = "در انتظار پرداخت";
                break;
            case "processing":
                ret = "در حال انجام";
                break;
            case "on-hold":
                ret = "نگهداشته شده";
                break;
            case "completed":
                ret = "انجام شده";
                break;
            case "cancelled":
                ret = "لغو شده";
                break;
            case "refunded":
                ret = "مسترد شده";
                break;
            case "failed":
                ret = "شکست خورده";
                break;
        }
        return ret;
    }

    public static void loadImage(ImageView imageView, String string, int placeholder) {
        Glide.with(mApplication.applicationContext)
                .load(string)
                .placeholder(placeholder)
                .priority(Priority.HIGH)
                .into(imageView);
    }

    public static void loadImage(ImageView imageView, String string, int error, final Callback mCallback) {
        Glide.with(mApplication.applicationContext)
                .load(string)
                .error(error)
                .priority(Priority.HIGH)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        mCallback.onError();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        mCallback.onSuccess();
                        return false;
                    }
                })
                .into(imageView);
    }

    public static void loadImage(ImageView imageView, String string) {
        Glide.with(mApplication.applicationContext)
                .load(string)
                .priority(Priority.HIGH)
                .into(imageView);
    }

    public static String fromHtml(String source) {
        if (TextUtils.isEmpty(source)) {
            return "";
        }
        if (source.contains("<ul")) {
            source.replaceAll("<ul", "<UL");
            source.replaceAll("<li", "<LI");
            source.replaceAll("</li>", "</LI>");
            source.replaceAll("</ul>", "</UL>");
            source.replaceAll("<ol", "<OL");
            source.replaceAll("</ol>", "</OL>");
            source.replaceAll("<dd", "<DD");
            source.replaceAll("</dd>", "</DD>");
        }
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(source.replaceAll("\\s+", " "), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(source.replaceAll("\\s+", " "));
        }
        String ret = result.toString();
        return ret;
    }
}
