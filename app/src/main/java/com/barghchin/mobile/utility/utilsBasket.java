package com.barghchin.mobile.utility;


import com.barghchin.mobile.ModelData.ShoppingBag;
import com.barghchin.mobile.ModelData.ShoppingBagModel;

import java.util.List;

public class utilsBasket {

    private ShoppingBagModel bagModel;

    public static utilsBasket newInstance() {
        return new utilsBasket();
    }


    public utilsBasket() {
        bagModel = ShoppingBagModel.fromString(MySharedPreferences.getInstance().getString(MySharedPreferences.SHOPPING_BAG, "[]"));
    }

    public List<ShoppingBag> getBaskets() {
        return bagModel.getBags();
    }

    public int getTotalBaskets() {
        int Total = 0;
        for (ShoppingBag bag : bagModel.getBags()) {
            Total += Integer.parseInt(bag.getPrice()) * bag.getCount();
        }
        return Total;
    }
}
