package com.barghchin.mobile.utility;

/**
 * Created by Ghanbarinia on 7/9/2017.
 */

public class Constant {
    public static final float AspectRatio = 0.5625f;//16:9

    public static final String DomainApp = "http://caviar.myaccount.ir";
    public static final String SecurityKey = "ABCDE";
    public static final String PathPlugin = "/wp-json/wc/v2/pluginName/";

    public static String ConsumerKey;//change after load init setting
    public static String ConsumerSecret;//change after load init setting

    public static final String[] Value_Countries = new String[]{"ایران"}, Key_Countries = new String[]{"IR"},

    Value_States = new String[]{
            "خوزستان",
            "تهران",
            "ایلام",
            "بوشهر",
            "اردبیل",
            "اصفهان",
            "یزد",
            "کرمانشاه",
            "کرمان",
            "همدان",
            "قزوین",
            "زنجان",
            "لرستان",
            "البرز",
            "آذربایجان شرقی",
            "آذربایجان غربی",
            "چهارمحال و بختیاری",
            "خراسان جنوبی",
            "خراسان رضوی",
            "خراسان جنوبی",
            "سمنان",
            "فارس",
            "قم",
            "کردستان",
            "کهگیلوییه و بویراحمد",
            "گلستان",
            "گیلان",
            "مازندران",
            "مرکزی",
            "هرمزگان",
            "سیستان و بلوچستان"
    }, Key_States = new String[]{
            "KHZ",
            "THR",
            "ILM",
            "BHR",
            "ADL",
            "ESF",
            "YZD",
            "KRH",
            "KRN",
            "HDN",
            "GZN",
            "ZJN",
            "LRS",
            "ABZ",
            "EAZ",
            "WAZ",
            "CHB",
            "SKH",
            "RKH",
            "NKH",
            "SMN",
            "FRS",
            "QHM",
            "KRD",
            "KBD",
            "GLS",
            "GIL",
            "MZN",
            "MKZ",
            "HRZ",
            "SBN"
    };
}
