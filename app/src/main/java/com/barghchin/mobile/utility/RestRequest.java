package com.barghchin.mobile.utility;

import android.support.v4.util.ArrayMap;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.barghchin.mobile.ModelData.VolleyResponseModel;
import com.barghchin.mobile.R;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by Ghanbarinia on 7/2/2017.
 */

public class RestRequest {

    private static RestRequest instance;
    private final int SOCKET_TIMEOUT_MS = 2500;

    public static RestRequest getInstance() {
        if (instance == null) {
            instance = new RestRequest();
        }
        return instance;
    }

    public void AsynchronousServiceCall(final String url, final callbackService callback) {
        AsynchronousServiceCall(url, Request.Method.GET, null, callback);
    }

    public void AsynchronousServiceCall(final String url, final int method, final callbackService callback) {
        AsynchronousServiceCall(url, method, null, callback);
    }

    /**
     * Asynchronous : When a task is executed asynchronously, you can directly switch to another task before the previous has been completed. One task does not depend on the other.
     */
    public void AsynchronousServiceCall(final String url, final int method, final Map<String, String> params, final callbackService callback) {
        final VolleyResponseModel responseModel = new VolleyResponseModel();
        if (Utils.checkInternetConnection(false)) {
            StringRequest strReq = new StringRequest(method, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    responseModel.setResponse(Utils.nullStrToEmpty(response));
                    if (callback != null) {
                        callback.success(responseModel);
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse networkResponse = error.networkResponse;
                    if (networkResponse != null && networkResponse.data != null) {
                        try {
                            responseModel.setStatusCode(networkResponse.statusCode);
                            responseModel.setResponse(new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers)));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    responseModel.setException(error);
                    if (callback != null) {
                        callback.error(responseModel);
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    if (params == null) {
                        return new ArrayMap<>();
                    }
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json");
                    return params;
                }
            };

            strReq.setRetryPolicy(retryPolicy);
            mApplication.getInstance().addToRequestQueue(strReq);
        } else {
            responseModel.setResponse(null);
            responseModel.setException(new VolleyError(mApplication.applicationContext.getString(R.string.no_connection)));
            if (callback != null) {
                callback.error(responseModel);
            }
        }
    }

    /***
     *Synchronous : When a task is executed synchronously, you wait for a task to be completed before moving on to another task. One task depends on the end of another
     */
    public VolleyResponseModel SynchronousServiceCall(final String url, final int method, final Map<String, String> params) {
        VolleyResponseModel responseModel = new VolleyResponseModel();
        if (Utils.checkInternetConnection(false)) {
            RequestFuture<String> future = RequestFuture.newFuture();

            StringRequest stringRequest = new StringRequest(method, url, future, future) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    if (params == null) {
                        return new ArrayMap<>();
                    }
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json");
                    return params;
                }
            };

            stringRequest.setRetryPolicy(retryPolicy);
            mApplication.getInstance().addToRequestQueue(stringRequest);

            try {
                String response = future.get(SOCKET_TIMEOUT_MS, TimeUnit.MILLISECONDS);
                responseModel.setResponse(response);
            } catch (Exception e) {
                responseModel.setException(e);
            }
        } else {
            responseModel.setResponse(null);
            responseModel.setException(new VolleyError(mApplication.applicationContext.getString(R.string.no_connection)));
        }
        return responseModel;
    }


    private RetryPolicy retryPolicy = new RetryPolicy() {
        @Override
        public int getCurrentTimeout() {
            return SOCKET_TIMEOUT_MS;
        }

        @Override
        public int getCurrentRetryCount() {
            return 3;
        }

        @Override
        public void retry(VolleyError error) throws VolleyError {
            Log.e("Alireza", "error time out");
        }
    };
}
