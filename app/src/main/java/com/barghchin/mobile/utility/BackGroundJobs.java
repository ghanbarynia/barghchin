package com.barghchin.mobile.utility;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.text.TextUtils;

import com.android.volley.Request;
import com.barghchin.mobile.ModelData.VolleyResponseModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.activity.ProductCategoryActivity;
import com.barghchin.mobile.activity.ProductDetailsActivity;
import com.barghchin.mobile.activity.SplashActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

import static android.content.Context.NOTIFICATION_SERVICE;

public class BackGroundJobs {

    public static volatile DispatchQueue mDispatchQueue = new DispatchQueue(BackGroundJobs.class.getSimpleName());

    public static void webservicePaymentMethods() {
        mDispatchQueue.postRunnable(new Runnable() {
            @Override
            public void run() {
                String urlPayment = new RequestQuery(RequestQuery.UrlOAuthType.GET_PAYMENT).GetUrl();
                VolleyResponseModel responseModel = RestRequest.getInstance().SynchronousServiceCall(urlPayment, Request.Method.GET, null);
                if (!TextUtils.isEmpty(responseModel.getResponse())) {
                    MySharedPreferences.getInstance().setPreference(MySharedPreferences.PaymentMethods, responseModel.getResponse());
                }
            }
        });
    }

    public static void webserviceGetShipping() {
        mDispatchQueue.postRunnable(new Runnable() {
            @Override
            public void run() {
                String Url_GetShipping = new RequestQuery(RequestQuery.UrlOAuthType.GET_SETTING)
                        .addRoute("checkout")
                        .addRoute("woocommerce_enable_guest_checkout")
                        .GetUrl();
                VolleyResponseModel responseModel = RestRequest.getInstance().SynchronousServiceCall(Url_GetShipping, Request.Method.GET, null);
                if (!TextUtils.isEmpty(responseModel.getResponse())) {
                    try {
                        JSONObject jsonObject = new JSONObject(responseModel.getResponse());
                        if (jsonObject.has("value") && !jsonObject.isNull("value")) {
                            MySharedPreferences.getInstance().setPreference(MySharedPreferences.PermissionPurchaseWithoutLogin, "yes".equalsIgnoreCase(jsonObject.getString("value")));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public static void Notification(final JSONArray notify) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < notify.length(); i++) {
                    try {
                        JSONObject jsonObject = notify.getJSONObject(i);
                        if (jsonObject.has("expierDate") && !jsonObject.isNull("expierDate")) {
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-ddHH:mm", Locale.getDefault());
                            sdf.setTimeZone(TimeZone.getTimeZone("Asia/Tehran"));
                            long time = sdf.parse(jsonObject.getString("expierDate")).getTime();
                            time = time - System.currentTimeMillis();
                            if (time > 0) {
                                int uniqueId = -1;
                                String title = null;
                                NotificationCompat.Builder notification = new NotificationCompat.Builder(mApplication.applicationContext);
                                notification.setAutoCancel(true);
                                notification.setSmallIcon(R.mipmap.ic_launcher);
                                if (jsonObject.has("title") && !jsonObject.isNull("title")) {
                                    title = jsonObject.getString("title");
                                    notification.setTicker(title);
                                }
                                notification.setWhen(System.currentTimeMillis());
                                if (jsonObject.has("titleForShow") && !jsonObject.isNull("titleForShow")) {
                                    notification.setContentTitle(jsonObject.getString("titleForShow"));
                                }
                                if (jsonObject.has("desc") && !jsonObject.isNull("desc")) {
                                    notification.setContentText(jsonObject.getString("desc"));
                                }
                                if (jsonObject.has("value") && !jsonObject.isNull("value")) {
                                    uniqueId = jsonObject.getInt("value");
                                    Class<?> cls = SplashActivity.class;
                                    Bundle extras = new Bundle();
                                    if (jsonObject.has("type") && !jsonObject.isNull("type")) {
                                        int type = jsonObject.getInt("type");
                                        if (type == 1) {
                                            cls = ProductCategoryActivity.class;
                                            extras.putInt("CategoryId", uniqueId);
                                            extras.putString("CategoryTitle", title);
                                        } else if (type == 2) {
                                            cls = ProductDetailsActivity.class;
                                            extras.putInt("ID", uniqueId);
                                        }
                                    }
                                    Intent intent = new Intent(mApplication.applicationContext, cls);
                                    intent.putExtras(extras);
                                    PendingIntent pendingIntent = PendingIntent.getActivity(mApplication.applicationContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                                    notification.setContentIntent(pendingIntent);
                                }
                                NotificationManager nm = (NotificationManager) mApplication.applicationContext.getSystemService(NOTIFICATION_SERVICE);
                                nm.notify(uniqueId, notification.build());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}
