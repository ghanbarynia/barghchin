package com.barghchin.mobile.utility;

import android.support.annotation.NonNull;
import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by A B B A S on 9/1/2017.
 */

public class OAuth  {

    private static final String HASH_ALGORITHM = "HMAC-SHA1";
    private static final String URL_ENCODING  = "UTF-8";


    public OAuth() {

    }

    private String percentEncode(String string ) throws UnsupportedEncodingException {
        return URLEncoder.encode(string, URL_ENCODING);
    }

    public void authorize(@NonNull RequestQuery query ) {
        Long tsLong = System.currentTimeMillis()/1000;
        String timestamp = tsLong.toString();

        UUID uuid = UUID.randomUUID();
        String nonce = uuid.toString();

        query.add("oauth_consumer_key", Constant.ConsumerKey);
        query.add("oauth_timestamp", timestamp);
        query.add("oauth_nonce", nonce);
        query.add("oauth_signature_method", HASH_ALGORITHM);
        query.add("oauth_version", "1.0");
        query.sortByKey();
        query.add("oauth_signature", generateSignature(query));
        query.sortByKey();
    }

    private String getSecret() {
        return Constant.ConsumerSecret + '&';
    }

    private String generateSignature( RequestQuery query ) {
        try {
            Map<String,String> encodedArguments = new HashMap<>();

            for (Map.Entry<String, String> parameter : query.getArguments().entrySet()) {
                encodedArguments.put( percentEncode(parameter.getKey()), percentEncode(parameter.getValue()));
            }

            Map<String,String> sortedEncodedArguments = new TreeMap<>(encodedArguments);
            StringBuilder queryStringBuilder = new StringBuilder();
            for (Map.Entry<String, String> parameter : sortedEncodedArguments.entrySet()) {
                queryStringBuilder
                        .append(parameter.getKey())
                        .append('=')
                        .append(parameter.getValue())
                        .append('&');
            }
            String queryString = queryStringBuilder.toString().substring(0,queryStringBuilder.length()-1); // Remove last &

            String signatureBase = query.type.toString() + "&" + percentEncode(query.getBareUrl()) + "&" + percentEncode(queryString);

            return SHA1(signatureBase, getSecret());
        }catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    private static String SHA1(String baseString, String keyString) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        byte[] keyBytes = keyString.getBytes();
        SecretKey secretKey = new SecretKeySpec(keyBytes, "HmacSHA1");

        Mac mac = Mac.getInstance("HmacSHA1");

        try {
            mac.init(secretKey);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }

        byte[] text = baseString.getBytes();

        return new String(Base64.encodeToString(mac.doFinal(text), Base64.DEFAULT)).trim();
    }
}
