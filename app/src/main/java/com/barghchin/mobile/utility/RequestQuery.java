package com.barghchin.mobile.utility;

import android.support.annotation.NonNull;

import com.android.volley.Request;
import com.barghchin.mobile.ModelData.VolleyResponseModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by A B B A S on 9/1/2017.
 */


public class RequestQuery {

    public RequestType type;
    public String endpoint;

    private Map<String, String> arguments = new HashMap<>();
    private List<String> routes;

    private String targetUrl = "";
    private String standardEndpoint = "/wp-json/wc/v2/";

    private final UrlOAuthType TypeRequest;

    public enum UrlOAuthType {
        GET_AppSetting,
        GET_PRODUCTS,
        GET_PRODUCTS_TOPSELLERS,
        GET_PRODUCTS_NEW,
        GET_PAYMENT,
        GET_SHIPPING,
        GET_ORDERS,
        GET_SHIPPING_ZONE,
        GET_COUPONS,
        GET_SETTING,
        GET_POSTS,
        POST_ORDER,
        POST_LOGIN,
        POST_REGISTER,
        GENERATE_OAUTH,
        POST_COMMENT,

    }

    public enum RequestType {
        GET, POST
    }

    public RequestQuery(@NonNull UrlOAuthType TypeRequest) {
        this.TypeRequest = TypeRequest;
        this.targetUrl = Constant.DomainApp;
        this.routes = new ArrayList<>();
    }

    public RequestQuery add(@NonNull String name, @NonNull String value) {
        if (arguments.containsKey(name)) {
            throw new IllegalArgumentException("Argument already exists");
        }
        arguments.put(name, value);
        return this;
    }

    public RequestQuery addRoute(@NonNull String route) {
        routes.add(route + "/");
        return this;
    }

    public void sortByKey() {
        arguments = new TreeMap<>(arguments);
    }

    public Map<String, String> getArguments() {
        return new TreeMap<>(arguments);
    }

    public String getBareUrl() {
        return targetUrl + standardEndpoint + endpoint;
    }

    private String getStringArguments() {

        StringBuilder builder = new StringBuilder();
        for (Map.Entry<?, ?> entry : arguments.entrySet()) {
            if (builder.length() > 0) {
                builder.append("&");
            }
            try {
                builder.append(String.format("%s=%s",
                        URLEncoder.encode(entry.getKey().toString(), "UTF-8"),
                        URLEncoder.encode(entry.getValue().toString(), "UTF-8")
                ));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return getBareUrl() + "?" + builder.toString();
    }

    public String GetUrl() {

        String routes = "/";

        for (String route : this.routes) {
            routes += route;
        }

        switch (TypeRequest) {
            case GET_AppSetting: {
                type = RequestType.GET;
                targetUrl = "http://boilerplate.ir"; //delete this line after install plugin
                standardEndpoint = Constant.PathPlugin;
                endpoint = "config";
                this.add("KeySucret", Constant.SecurityKey);
                //add to "this" default params
                return getStringArguments();
            }
            case GET_POSTS: {
                type = RequestType.GET;

                standardEndpoint = "/wp-json/wp/v2/";

                endpoint = "posts" + routes;

                this.add("status", "publish");
                this.add("orderby", "date");
                this.add("order", "desc");
                this.add("_embed", "1");

                //add to "this" default params
                new OAuth().authorize(this);
                return getStringArguments();
            }

            case GET_PRODUCTS: {
                type = RequestType.GET;
                endpoint = "products" + routes;
                //add to "this" default params
                new OAuth().authorize(this);
                return getStringArguments();
            }
            case GET_PRODUCTS_TOPSELLERS: {
                type = RequestType.GET;
                endpoint = "reports/top_sellers";
                //add to "this" default params
                this.add("status", "publish");
                new OAuth().authorize(this);
                return getStringArguments();
            }
            case GET_PRODUCTS_NEW: {
                type = RequestType.GET;
                endpoint = "products";
                //add to "this" default params
                this.add("status", "publish");
                this.add("orderby", "date");
                this.add("order", "desc");
                new OAuth().authorize(this);
                return getStringArguments();
            }
            case GET_ORDERS: {
                type = RequestType.GET;
                endpoint = "orders" + routes;
                //add to "this" default params
                this.add("orderby", "date");
                this.add("order", "desc");
                new OAuth().authorize(this);
                return getStringArguments();
            }
            case GET_PAYMENT: {
                type = RequestType.GET;
                endpoint = "payment_gateways";
                //add to "this" default params
                new OAuth().authorize(this);
                return getStringArguments();
            }
            case GET_SHIPPING: {
                type = RequestType.GET;
                endpoint = "shipping_methods";
                //add to "this" default params
                new OAuth().authorize(this);
                return getStringArguments();
            }
            case GET_SHIPPING_ZONE: {
                type = RequestType.GET;
                endpoint = "shipping/zones" + routes;
                //add to "this" default params
                new OAuth().authorize(this);
                return getStringArguments();
            }
            case GET_COUPONS: {
                type = RequestType.GET;
                endpoint = "coupons";
                //add to "this" default params
                new OAuth().authorize(this);
                return getStringArguments();
            }
            case GET_SETTING: {
                type = RequestType.GET;
                endpoint = "settings" + routes;
                //add to "this" default params
                new OAuth().authorize(this);
                return getStringArguments();
            }
            case POST_ORDER: {
                type = RequestType.POST;
                endpoint = "orders";
                //add to "this" default params
                new OAuth().authorize(this);
                return getStringArguments();
            }
            case GENERATE_OAUTH: {
                type = RequestType.POST;
                standardEndpoint = "/api/";
                endpoint = "get_nonce";
                this.add("controller", "user");
                return getStringArguments();
            }
            case POST_LOGIN: { //add -> username | password
                type = RequestType.POST;
                standardEndpoint = "/api/";
                endpoint = "user/generate_auth_cookie";

                RequestQuery requestQuery = new RequestQuery(UrlOAuthType.GENERATE_OAUTH);
                String url = requestQuery.add("method", "generate_auth_cookie").GetUrl();

                VolleyResponseModel responseModel = RestRequest.getInstance().SynchronousServiceCall(url, Request.Method.GET, null);
                String nonce = "";
                try {
                    JSONObject jsonObject = new JSONObject(responseModel.getResponse());
                    if (jsonObject.has("nonce") && !jsonObject.isNull("nonce")) {
                        nonce = jsonObject.getString("nonce");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // call api

                this.add("nonce", nonce);
                this.add("insecure", "cool");
                //add to "this" default params
                return getStringArguments();
            }
            case POST_REGISTER: { //add -> username | email | display_name | user_pass
                type = RequestType.POST;
                standardEndpoint = "/api/";
                endpoint = "user/register";

                RequestQuery requestQuery = new RequestQuery(UrlOAuthType.GENERATE_OAUTH);
                String url = requestQuery.add("method", "register").GetUrl();

                // call api
                VolleyResponseModel responseModel = RestRequest.getInstance().SynchronousServiceCall(url, Request.Method.GET, null);
                String nonce = "";
                try {
                    JSONObject jsonObject = new JSONObject(responseModel.getResponse());
                    if (jsonObject.has("nonce") && !jsonObject.isNull("nonce")) {
                        nonce = jsonObject.getString("nonce");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                this.add("nonce", nonce);
                this.add("insecure", "cool");
                this.add("notify", "both");
                //add to "this" default params
                return getStringArguments();
            }
            case POST_COMMENT: { //add -> cookie | post_id | content |
                type = RequestType.POST;
                standardEndpoint = "/api/";
                endpoint = "user/post_comment";

                this.add("insecure", "cool");
                this.add("comment_status", "1");
                //add to "this" default params
                return getStringArguments();
            }
            default:
                break;
        }

        return "";
    }
}
