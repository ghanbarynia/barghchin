package com.barghchin.mobile.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.android.volley.Request;
import com.barghchin.mobile.utility.BackGroundJobs;
import com.barghchin.mobile.utility.RequestQuery;
import com.barghchin.mobile.utility.RestRequest;

import org.json.JSONObject;

/**
 * Created by Ghanbarinia on 9/12/2017.
 */

public class checkService extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String url = new RequestQuery(RequestQuery.UrlOAuthType.GET_AppSetting).GetUrl();
                String response = RestRequest.getInstance().SynchronousServiceCall(url, Request.Method.GET, null).getResponse();
                if (!TextUtils.isEmpty(response)) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has("notify") && !jsonObject.isNull("notify")) {
                            BackGroundJobs.Notification(jsonObject.getJSONArray("notify"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}
