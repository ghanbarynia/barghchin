package com.barghchin.mobile.activity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.barghchin.mobile.Adapter.UserCommentsAdapter;
import com.barghchin.mobile.ModelData.UserCommentModel;
import com.barghchin.mobile.ModelData.VolleyResponseModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.utility.RequestQuery;
import com.barghchin.mobile.utility.RestRequest;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.callbackService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.barghchin.mobile.R.id.Rb_comment;

public class UserCommentsActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView mRecyclerView;
    private MaterialDialog pDialog;
    private UserCommentsActivity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_comments);
        initView();
        LoadData();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initView() {
        mActivity = this;

        findViewById(R.id.TopToolbar).setBackgroundColor(Utils.getAppThemeColor());
        TextView mTvTitle = (TextView) findViewById(R.id.Tv_Title);
        mTvTitle.setText("نظرات");

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_UserCommentsActivity);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));

        findViewById(R.id.Iv_Back).setOnClickListener(this);
        findViewById(R.id.ic_shopping_cart).setOnClickListener(this);
        findViewById(R.id.ic_menu_search).setOnClickListener(this);
        findViewById(R.id.fab_UserCommentsActivity).setOnClickListener(this);
    }

    private void LoadData() {
        if (pDialog == null) {
            pDialog = Utils.CustomDialog(mActivity).content(R.string.please_wait).progress(true, 0).show();
        } else {
            pDialog.show();
        }

        String ID = "-1";
        Object id = getIntent().getExtras().get("ID");
        if (id instanceof Integer) {
            ID = String.valueOf(id);
        } else if (id instanceof String) {
            ID = (String) id;
        }

        String URL = new RequestQuery(RequestQuery.UrlOAuthType.GET_PRODUCTS)
                .addRoute(ID)
                .addRoute("reviews")
                .add("status", "publish").GetUrl();

        RestRequest.getInstance().AsynchronousServiceCall(URL, new callbackService() {
            @Override
            public void success(VolleyResponseModel callback) {
                List<UserCommentModel> mData = new ArrayList<>();
                try {
                    JSONArray jsonArray = new JSONArray(callback.getResponse());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        UserCommentModel commentModel = new UserCommentModel();

                        if (jsonObject.has("id") && !jsonObject.isNull("id")) {
                            commentModel.setId(jsonObject.getInt("id"));
                        }

                        if (jsonObject.has("date_created") && !jsonObject.isNull("date_created")) {
                            commentModel.setDate_created(jsonObject.getString("date_created"));
                        }

                        if (jsonObject.has("review") && !jsonObject.isNull("review")) {
                            commentModel.setReview(jsonObject.getString("review"));
                        }

                        if (jsonObject.has("name") && !jsonObject.isNull("name")) {
                            commentModel.setName(jsonObject.getString("name"));
                        }

                        if (jsonObject.has("verified") && !jsonObject.isNull("verified")) {
                            commentModel.setVerified(jsonObject.getBoolean("verified"));
                        }
                        if (commentModel.isVerified()) {
                            mData.add(commentModel);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mRecyclerView.setAdapter(new UserCommentsAdapter(mActivity, mData));
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
            }

            @Override
            public void error(VolleyResponseModel callback) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Utils.Toast("error");
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_UserCommentsActivity:
                Utils.CustomDialog(mActivity)
                        .title("دیدگاه شما:")
                        .titleColor(Color.parseColor("#e12929"))
                        .positiveColor(Color.BLACK)
                        .negativeColor(Color.BLACK)
                        .positiveText("ارسال دیدگاه")
                        .negativeText("انصراف")
                        .customView(R.layout.dialog_comment, false)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                EditText mEtName = (EditText) dialog.findViewById(R.id.ET_Name);
                                EditText mEtEmail = (EditText) dialog.findViewById(R.id.ET_Email);
                                EditText mEtcomment = (EditText) dialog.findViewById(R.id.ET_Comment);
                                RatingBar mRbComment = (RatingBar) dialog.findViewById(Rb_comment);
                            }
                        })
                        .show();
                break;
            case R.id.ic_shopping_cart:
                Utils.startShoppingBagActivity(mActivity);
                break;
            case R.id.ic_menu_search:
                Utils.startSearchActivity(mActivity);
                break;
            case R.id.Iv_Back:
                onBackPressed();
                break;
        }
    }
}
