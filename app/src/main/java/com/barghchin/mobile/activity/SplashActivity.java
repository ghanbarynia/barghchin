package com.barghchin.mobile.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.barghchin.mobile.Interface.Callback;
import com.barghchin.mobile.ModelData.VolleyResponseModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.service.checkService;
import com.barghchin.mobile.utility.BackGroundJobs;
import com.barghchin.mobile.utility.Constant;
import com.barghchin.mobile.utility.MySharedPreferences;
import com.barghchin.mobile.utility.RequestQuery;
import com.barghchin.mobile.utility.RestRequest;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.callbackService;
import com.barghchin.mobile.utility.mApplication;

import org.json.JSONObject;

public class SplashActivity extends AppCompatActivity {

    private SplashActivity mActivity;
    private MaterialDialog mDialog;
    private ImageView mIvCenter;
    private ProgressBar mProgressBar;
    private View mroot;
    private TextView mTvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initViews();
    }

    private void StartService() {
        Intent intent = new Intent(this, checkService.class);
        PendingIntent pi = PendingIntent.getBroadcast(this, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) this.getSystemService(ALARM_SERVICE);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (6 * 60 * 60 * 1000), 6 * 60 * 60 * 1000, pi);
    }

    private void initViews() {
        mActivity = this;
        mIvCenter = (ImageView) findViewById(R.id.Iv_Center);
        mProgressBar = (ProgressBar) findViewById(R.id.Pb_Center);
        mTvTitle = (TextView) findViewById(R.id.TV_Title);
        mroot = findViewById(R.id.rootSplashActivity);
        mTvTitle.setTypeface(mApplication.IRANSansBold);
        int size = mApplication.mPoint.x / 2;
        mIvCenter.setLayoutParams(new FrameLayout.LayoutParams(size, size));
        setColor();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadData();
    }

    private void setColor() {
        mProgressBar.getIndeterminateDrawable().setColorFilter(Utils.getAppThemeColor(), PorterDuff.Mode.MULTIPLY);
        mroot.setBackgroundColor(Utils.getAppThemeColor());
    }

    private void loadData() {
        if (Utils.checkInternetConnection(false)) {
            if (mDialog == null) {
                mDialog = Utils.CustomDialog(mActivity).cancelable(false).content(R.string.please_wait).progress(true, 0).show();
            } else {
                mDialog.show();
            }

            String url = new RequestQuery(RequestQuery.UrlOAuthType.GET_AppSetting).GetUrl();
            RestRequest.getInstance().AsynchronousServiceCall(url, new callbackService() {
                @Override
                public void success(final VolleyResponseModel callback) {
                    Log.e("Alireza", "2");
                    if (mDialog != null && mDialog.isShowing()) {
                        mDialog.dismiss();
                    }
                    try {
                        JSONObject jsonObject = new JSONObject(callback.getResponse());
                        MySharedPreferences.getInstance().setPreference(MySharedPreferences.MainContent, callback.getResponse());
                        if (jsonObject.has("error") && !jsonObject.isNull("error") && jsonObject.getBoolean("error") && jsonObject.has("errorText") && !jsonObject.isNull("errorText")) {
                            Log.e("Alireza", "3");
                            Utils.CustomDialog(mActivity)
                                    .cancelable(false)
                                    .content(jsonObject.getString("errorText"))
                                    .positiveText("خروج از برنامه")
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            onBackPressed();
                                        }
                                    })
                                    .show();
                        } else {
                            Log.e("Alireza", "4");
                            if (jsonObject.has("consumer_key") && !jsonObject.isNull("consumer_key")) {
                                Constant.ConsumerKey = jsonObject.getString("consumer_key");
                            }
                            if (jsonObject.has("consumer_secret") && !jsonObject.isNull("consumer_secret")) {
                                Constant.ConsumerSecret = jsonObject.getString("consumer_secret");
                            }
                            StartService();
                            BackGroundJobs.webserviceGetShipping();
                            int SETTING_VERSION = MySharedPreferences.getInstance().getInt(MySharedPreferences.SETTING_VERSION, 0);
                            if (SETTING_VERSION == 0 || (jsonObject.has("settingVersion") && !jsonObject.isNull("settingVersion") && jsonObject.getInt("settingVersion") != SETTING_VERSION)) {
                                Log.e("Alireza", "5");

                                if (jsonObject.has("storeNameApp") && !jsonObject.isNull("storeNameApp")) {
                                    MySharedPreferences.getInstance().setPreference(MySharedPreferences.STORE_NAME_APP, jsonObject.getString("storeNameApp"));
                                }
                                if (jsonObject.has("storeLogoApp") && !jsonObject.isNull("storeLogoApp")) {
                                    MySharedPreferences.getInstance().setPreference(MySharedPreferences.STORE_LOGO_APP, jsonObject.getString("storeLogoApp"));
                                }
                                if (jsonObject.has("storeColor") && !jsonObject.isNull("storeColor")) {
                                    MySharedPreferences.getInstance().setPreference(MySharedPreferences.STORE_COLOR, jsonObject.getString("storeColor"));
                                }
                                if (jsonObject.has("settingVersion") && !jsonObject.isNull("settingVersion")) {
                                    MySharedPreferences.getInstance().setPreference(MySharedPreferences.SETTING_VERSION, jsonObject.getInt("settingVersion"));
                                }
                            }
                            if (jsonObject.has("notify") && !jsonObject.isNull("notify")) {
                                BackGroundJobs.Notification(jsonObject.getJSONArray("notify"));
                            }
                            Log.e("Alireza", "6");
                            setColor();
                            mProgressBar.setVisibility(View.VISIBLE);
                            mTvTitle.setText(MySharedPreferences.getInstance().getString(MySharedPreferences.STORE_NAME_APP, ""));
                            Utils.loadImage(mIvCenter, MySharedPreferences.getInstance().getString(MySharedPreferences.STORE_LOGO_APP, "error"), R.drawable.place_holder, new Callback() {
                                @Override
                                public void onSuccess() {
                                    Log.e("Alireza", "7");
                                    mProgressBar.setVisibility(View.GONE);
                                    mApplication.applicationHandler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            startActivity(new Intent(mActivity, MainActivity.class));
                                            finish();
                                        }
                                    }, 3000);
                                }

                                @Override
                                public void onError() {
                                    Log.e("Alireza", "8");
                                    startActivity(new Intent(mActivity, MainActivity.class));
                                    finish();
                                }
                            });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void error(VolleyResponseModel callback) {
                    Utils.CustomDialog(mActivity)
                            .cancelable(false)
                            .content("مشکل در برقراری ارتباط با سرور لطفا دوباره تلاش کنید")
                            .positiveText("بله")
                            .negativeText("خیر")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    loadData();
                                }
                            })
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    onBackPressed();
                                }
                            })
                            .show();
                    if (mDialog != null && mDialog.isShowing()) {
                        mDialog.dismiss();
                    }
                }
            });
        } else {
            Utils.CustomDialog(mActivity)
                    .cancelable(false)
                    .content("اتصال شما با اینترنت برقرار نیست، آیا مایل به روشن کردن wifi یا data خود هستید؟")
                    .positiveText("بله")
                    .negativeText("خیر")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            try {
                                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                            } catch (Exception e) {
                                startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
//                            startActivity(new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK));
                            }
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            onBackPressed();
                        }
                    })
                    .show();
        }
    }
}
