package com.barghchin.mobile.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.barghchin.mobile.Adapter.CustomAdapter_Categories_Product_Linear;
import com.barghchin.mobile.Adapter.MainActivityAdapter;
import com.barghchin.mobile.ModelData.MainActivityModel;
import com.barghchin.mobile.ModelData.ModuleDataModel;
import com.barghchin.mobile.ModelData.VolleyResponseModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.utility.RequestQuery;
import com.barghchin.mobile.utility.RestRequest;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.callbackService;
import com.barghchin.mobile.utility.mApplication;

import org.json.JSONArray;
import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CompleteListActivity extends AppCompatActivity implements View.OnClickListener {

    private CompleteListActivity mActivity;
    private MaterialDialog pDialog;
    private TextView mTvTitle;
    private MainActivityModel mData = new MainActivityModel();
    private CustomAdapter_Categories_Product_Linear mAdapter;
    private RecyclerView mRecyclerView;
    private boolean flag = false;
    private int page = 2;
    public static final int LatestProducts = 1, Best_ellingProducts = 2, CompleteList = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_category);
        initView();
        LoadData();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ic_shopping_cart:
                Utils.startShoppingBagActivity(mActivity);
                break;
            case R.id.ic_menu_search:
                Utils.startSearchActivity(mActivity);
                break;
            case R.id.Iv_Back:
                onBackPressed();
                break;
        }
    }

    private void initView() {
        mActivity = this;
        findViewById(R.id.Iv_Back).setOnClickListener(this);
        findViewById(R.id.ic_shopping_cart).setOnClickListener(this);
        findViewById(R.id.ic_menu_search).setOnClickListener(this);
        mTvTitle = (TextView) findViewById(R.id.Tv_Title);
        findViewById(R.id.TopToolbar).setBackgroundColor(Utils.getAppThemeColor());
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_ProductCategoryActivity);
        mTvTitle.setTypeface(mApplication.IRANSansBold);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setAutoMeasureEnabled(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setLayoutManager(layoutManager);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                if (dy > 0) {
                    if ((visibleItemCount == (totalItemCount - firstVisibleItem)) && flag && mData.getModule_data().size() > 0) {
                        flag = false;
                        ShowDialog();
                        RestRequest.getInstance().AsynchronousServiceCall(GetUrl(), new callbackService() {
                            @Override
                            public void success(VolleyResponseModel callback) {
                                try {
                                    JSONArray jsonArray = new JSONArray(callback.getResponse());
                                    if (jsonArray.length() == 10) {
                                        flag = true;
                                        page++;
                                    }
                                    int positionStart = mData.getModule_data().size();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                                        ModuleDataModel dataModel = new ModuleDataModel();

                                        if (jsonObject.has("id") && !jsonObject.isNull("id")) {
                                            dataModel.setId(jsonObject.getInt("id"));
                                        }

                                        if (jsonObject.has("name") && !jsonObject.isNull("name")) {
                                            dataModel.setTitle(jsonObject.getString("name"));
                                        }

                                        if (jsonObject.has("price") && !jsonObject.isNull("price")) {
                                            dataModel.setPrice(jsonObject.getString("price"));
                                        }

                                        if (jsonObject.has("short_description") && !jsonObject.isNull("short_description")) {
                                            dataModel.setShort_description(Utils.fromHtml(jsonObject.getString("short_description")));
                                        }

                                        if (jsonObject.has("images") && !jsonObject.isNull("images")) {
                                            JSONArray images = jsonObject.getJSONArray("images");
                                            if (images.getJSONObject(0).has("src") && !images.getJSONObject(0).isNull("src")) {
                                                dataModel.setImage(images.getJSONObject(0).getString("src"));
                                            }
                                        }
                                        mData.getModule_data().add(dataModel);
                                    }
                                    mAdapter.notifyItemRangeInserted(positionStart, mData.getModule_data().size());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                DismissDialog();
                            }

                            @Override
                            public void error(VolleyResponseModel callback) {
                                DismissDialog();
                            }
                        });
                    }
                }
            }
        });
    }

    private void LoadData() {
        Intent intent = getIntent();
        mTvTitle.setText(intent.getStringExtra("ModelTitle"));
        int Model = intent.getIntExtra("Model", -1);
        if (Model == CompleteList) {
            mData = (MainActivityModel) intent.getSerializableExtra("data");
            if (mData.getModule_data().size() == 10) {
                flag = true;
            }
            mRecyclerView.setAdapter(mAdapter = new CustomAdapter_Categories_Product_Linear(mActivity, mData));
            mAdapter.notifyDataSetChanged();
        } else if (Model == LatestProducts) {
            String URL = new RequestQuery(RequestQuery.UrlOAuthType.GET_PRODUCTS_TOPSELLERS).GetUrl();
            RestRequest.getInstance().AsynchronousServiceCall(URL,new callbackService(){
                @Override
                public void success(VolleyResponseModel callback) {
                    super.success(callback);
                }

                @Override
                public void error(VolleyResponseModel callback) {
                    super.error(callback);
                }
            });
        } else if (Model == Best_ellingProducts) {
            String URL = new RequestQuery(RequestQuery.UrlOAuthType.GET_PRODUCTS_NEW).GetUrl();
            RestRequest.getInstance().AsynchronousServiceCall(URL,new callbackService(){
                @Override
                public void success(VolleyResponseModel callback) {
                    super.success(callback);
                }

                @Override
                public void error(VolleyResponseModel callback) {
                    super.error(callback);
                }
            });
        }
    }

    private String GetUrl() {
        switch (getIntent().getIntExtra("TYPE", -1)) {
            case MainActivityAdapter.VIEW_TYPES.Best_sellingProducts:
                return new RequestQuery(RequestQuery.UrlOAuthType.GET_PRODUCTS_TOPSELLERS)
                        .add("page", String.valueOf(page))
                        .GetUrl();
            case MainActivityAdapter.VIEW_TYPES.AuctionProducts:
                return new RequestQuery(RequestQuery.UrlOAuthType.GET_PRODUCTS_NEW)
                        .add("on_sale", "true")
                        .add("page", String.valueOf(page))
                        .GetUrl();
            case MainActivityAdapter.VIEW_TYPES.NewProducts:
                return new RequestQuery(RequestQuery.UrlOAuthType.GET_PRODUCTS_NEW)
                        .add("page", String.valueOf(page))
                        .GetUrl();
            case MainActivityAdapter.VIEW_TYPES.SpecialCategoryProducts:
                return new RequestQuery(RequestQuery.UrlOAuthType.GET_PRODUCTS_NEW)
                        .add("category", getIntent().getStringExtra("LinkId"))
                        .add("page", String.valueOf(page))
                        .GetUrl();
            case MainActivityAdapter.VIEW_TYPES.FeaturedProducts:
                return new RequestQuery(RequestQuery.UrlOAuthType.GET_PRODUCTS_NEW)
                        .add("featured", "true")
                        .add("page", String.valueOf(page))
                        .GetUrl();
            case MainActivityAdapter.VIEW_TYPES.Category:
                return new RequestQuery(RequestQuery.UrlOAuthType.GET_PRODUCTS)
                        .addRoute("categories")
                        .add("parent", "0")
                        .add("page", String.valueOf(page))
                        .GetUrl();
        }
        return "";
    }


    private void ShowDialog() {
        if (pDialog == null) {
            pDialog = Utils.CustomDialog(mActivity).content(R.string.please_wait).progress(true, 0).show();
        } else {
            pDialog.show();
        }
    }

    private void DismissDialog() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }
}
