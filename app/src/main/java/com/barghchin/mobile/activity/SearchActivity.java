package com.barghchin.mobile.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.barghchin.mobile.Adapter.MainActivityAdapter;
import com.barghchin.mobile.ModelData.MainActivityModel;
import com.barghchin.mobile.ModelData.ModuleDataModel;
import com.barghchin.mobile.ModelData.VolleyResponseModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.utility.RequestQuery;
import com.barghchin.mobile.utility.RestRequest;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.callbackService;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SearchActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private MaterialDialog pDialog;
    private SearchActivity mActivity;
    private List<MainActivityModel> mData = new ArrayList<>();
    private MainActivityAdapter mAdapter;
    private boolean flag = false;
    private int page = 2;
    private String query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        initView();
    }

    private void initView() {
        mActivity = this;
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_SearchActivity);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setAutoMeasureEnabled(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setLayoutManager(layoutManager);
        MainActivityModel category_Model2 = new MainActivityModel();
        category_Model2.setType(MainActivityAdapter.VIEW_TYPES.Category_Product_Linear);
        mData.add(category_Model2);
        mRecyclerView.setAdapter(mAdapter = new MainActivityAdapter(mActivity, mData));

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                if (dy > 0) {
                    if ((visibleItemCount == (totalItemCount - firstVisibleItem)) && flag && mData.get(0).getModule_data().size() > 0) {
                        flag = false;
                        String URL = new RequestQuery(RequestQuery.UrlOAuthType.GET_PRODUCTS_NEW).add("page", String.valueOf(page)).add("search", query).GetUrl();
                        ShowDialog();
                        RestRequest.getInstance().AsynchronousServiceCall(URL, new callbackService() {
                            @Override
                            public void success(VolleyResponseModel callback) {
                                try {
                                    JSONArray jsonArray = new JSONArray(callback.getResponse());
                                    if (jsonArray.length() == 10) {
                                        flag = true;
                                        page++;
                                    }
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                                        ModuleDataModel dataModel = new ModuleDataModel();

                                        if (jsonObject.has("id") && !jsonObject.isNull("id")) {
                                            dataModel.setId(jsonObject.getInt("id"));
                                        }

                                        if (jsonObject.has("name") && !jsonObject.isNull("name")) {
                                            dataModel.setTitle(jsonObject.getString("name"));
                                        }

                                        if (jsonObject.has("price") && !jsonObject.isNull("price")) {
                                            dataModel.setPrice(jsonObject.getString("price"));
                                        }

                                        if (jsonObject.has("short_description") && !jsonObject.isNull("short_description")) {
                                            dataModel.setShort_description(Utils.fromHtml(jsonObject.getString("short_description")));
                                        }

                                        if (jsonObject.has("images") && !jsonObject.isNull("images")) {
                                            JSONArray images = jsonObject.getJSONArray("images");
                                            if (images.getJSONObject(0).has("src") && !images.getJSONObject(0).isNull("src")) {
                                                dataModel.setImage(images.getJSONObject(0).getString("src"));
                                            }
                                        }
                                        mData.get(0).getModule_data().add(dataModel);
                                    }
                                    mAdapter.notifyItemChanged(0);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                DismissDialog();
                            }

                            @Override
                            public void error(VolleyResponseModel callback) {
                                DismissDialog();
                            }
                        });
                    }
                }
            }
        });


        MaterialSearchView mSearchView = (MaterialSearchView) findViewById(R.id.search_view);
        mSearchView.setVoiceSearch(true);
        mSearchView.setEllipsize(false);
        mSearchView.showSearch(false);
        mSearchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query2) {
                try {
                    query2 = query2.replace(" ", "_");
                    query = URLEncoder.encode(query2, "utf-8");
                } catch (Exception e) {
                    query = query2;
                }
                ShowDialog();
                String URL = new RequestQuery(RequestQuery.UrlOAuthType.GET_PRODUCTS_NEW).add("page", "1").add("search", query).GetUrl();
                RestRequest.getInstance().AsynchronousServiceCall(URL, new callbackService() {
                    @Override
                    public void success(VolleyResponseModel callback) {
                        mData.get(0).getModule_data().clear();
                        mAdapter.notifyItemChanged(0);
                        try {
                            JSONArray jsonArray = new JSONArray(callback.getResponse());
                            if (jsonArray.length() == 10) {
                                flag = true;
                            }

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                ModuleDataModel dataModel = new ModuleDataModel();

                                if (jsonObject.has("id") && !jsonObject.isNull("id")) {
                                    dataModel.setId(jsonObject.getInt("id"));
                                }

                                if (jsonObject.has("name") && !jsonObject.isNull("name")) {
                                    dataModel.setTitle(jsonObject.getString("name"));
                                }

                                if (jsonObject.has("price") && !jsonObject.isNull("price")) {
                                    dataModel.setPrice(jsonObject.getString("price"));
                                }

                                if (jsonObject.has("short_description") && !jsonObject.isNull("short_description")) {
                                    dataModel.setShort_description(Utils.fromHtml(jsonObject.getString("short_description")));
                                }

                                if (jsonObject.has("images") && !jsonObject.isNull("images")) {
                                    JSONArray images = jsonObject.getJSONArray("images");
                                    if (images.getJSONObject(0).has("src") && !images.getJSONObject(0).isNull("src")) {
                                        dataModel.setImage(images.getJSONObject(0).getString("src"));
                                    }
                                }
                                mData.get(0).getModule_data().add(dataModel);
                            }
                            mAdapter.notifyItemChanged(0);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        DismissDialog();
                    }

                    @Override
                    public void error(VolleyResponseModel callback) {
                        DismissDialog();
                    }
                });
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        mSearchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                onBackPressed();
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void ShowDialog() {
        if (pDialog == null) {
            pDialog = Utils.CustomDialog(mActivity).content(R.string.please_wait).progress(true, 0).show();
        } else {
            pDialog.show();
        }
    }

    private void DismissDialog() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }
}
