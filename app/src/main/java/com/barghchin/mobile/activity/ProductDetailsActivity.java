package com.barghchin.mobile.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.barghchin.mobile.Adapter.SliderAdapter;
import com.barghchin.mobile.Library.viewPager.LoopViewPager;
import com.barghchin.mobile.ModelData.Attributes;
import com.barghchin.mobile.ModelData.ModuleDataModel;
import com.barghchin.mobile.ModelData.ShoppingBag;
import com.barghchin.mobile.ModelData.VolleyResponseModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.utility.RequestQuery;
import com.barghchin.mobile.utility.RestRequest;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.callbackService;
import com.barghchin.mobile.utility.mApplication;
import com.barghchin.mobile.view.ExpandableTextView;
import com.like.LikeButton;
import com.like.OnLikeListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import cn.iwgang.countdownview.CountdownView;
import me.relex.circleindicator.CircleIndicator;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProductDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private ProductDetailsActivity mActivity;
    private ImageView mIvBack, mIvShoppingCart;
    private MaterialDialog pDialog;
    private LoopViewPager mViewPager;
    private CircleIndicator mIndicator;
    private TextView mTvName, mTvPrice, mTvSpecial;
    private ExpandableTextView mExpandTextView;
    private CountdownView mCountdownView;
    private RatingBar mRatingBar;
    private ModuleDataModel dataModel;
    private View mLayoutAddShopping;
    private CardView mLayoutVariable;
    private int ID, pageVariations = 1, position_Variation_selected = -1;
    private List<ModuleDataModel> modelsVariations = new ArrayList<>();
    private LikeButton mIvFavorite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        initView();
        LoadData();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initView() {
        mActivity = this;
        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        AppBarLayout mAppbarLayout = (AppBarLayout) findViewById(R.id.appbarLayout);
        mIvBack = (ImageView) findViewById(R.id.Iv_Back);
        mIvShoppingCart = (ImageView) findViewById(R.id.iv_shopping_cart);
        mViewPager = (LoopViewPager) findViewById(R.id.viewpager);
        mIndicator = (CircleIndicator) findViewById(R.id.indicator);
        mIvBack.setOnClickListener(this);
        mIvShoppingCart.setOnClickListener(this);
        findViewById(R.id.Iv_Share).setOnClickListener(this);
        mIvFavorite = (LikeButton) findViewById(R.id.Iv_Favorite);
        mIvFavorite.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
                mApplication.db.insertProduct(dataModel.toContentValues());
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                mApplication.db.deleteProduct(String.valueOf(dataModel.getId()));
            }
        });
        mLayoutVariable = (CardView) findViewById(R.id.Layout_variable);
        mLayoutAddShopping = findViewById(R.id.Layout_Add_Shopping);
        mLayoutAddShopping.setOnClickListener(this);
        findViewById(R.id.Layout_Comment).setOnClickListener(this);
        findViewById(R.id.Layout_Details).setOnClickListener(this);
        mTvName = (TextView) findViewById(R.id.Tv_Name);
        mTvSpecial = (TextView) findViewById(R.id.Tv_Special);
        mRatingBar = (RatingBar) findViewById(R.id.ratingBar);
        mTvPrice = (TextView) findViewById(R.id.Tv_Price);
        mExpandTextView = (ExpandableTextView) findViewById(R.id.expand_text_view);
        mCountdownView = (CountdownView) findViewById(R.id.cv_countdownView);

        mExpandTextView.setOnExpandStateChangeListener(new ExpandableTextView.OnExpandStateChangeListener() {
            @Override
            public void onExpandStateChanged(TextView textView, boolean isExpanded) {
                if (isExpanded) {
                    mExpandTextView.setText(dataModel.getDescription());
                } else {
                    mExpandTextView.setText(dataModel.getShort_description());
                }
            }
        });

        collapsingToolbarLayout.setTitle(" ");

        mTvName.setTypeface(mApplication.IRANSansBold);

        mAppbarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) - appBarLayout.getTotalScrollRange() == 0) {
                    mIvBack.setColorFilter(Color.WHITE);
                    mIvShoppingCart.setColorFilter(Color.WHITE);
                } else {
                    mIvBack.setColorFilter(Color.BLACK);
                    mIvShoppingCart.setColorFilter(Color.BLACK);
                }
            }
        });
    }

    private void LoadData() {
        if (pDialog == null) {
            pDialog = Utils.CustomDialog(mActivity).content(R.string.please_wait).progress(true, 0).show();
        } else {
            pDialog.show();
        }
        ID = -1;
        Object id = getIntent().getExtras().get("ID");
        if (id instanceof Integer) {
            ID = (int) id;
        } else if (id instanceof String) {
            ID = Integer.parseInt((String) id);
        }

        String URL = new RequestQuery(RequestQuery.UrlOAuthType.GET_PRODUCTS).addRoute(String.valueOf(ID)).GetUrl();
        RestRequest.getInstance().AsynchronousServiceCall(URL, new callbackService() {
            @Override
            public void success(VolleyResponseModel callback) {
                try {
                    dataModel = Utils.serializeJsonProduct(new JSONObject(callback.getResponse()));

                    mIvFavorite.setLiked(mApplication.db.isFavorite(dataModel.getId()));

                    mTvName.setText(dataModel.getName());
                    mTvPrice.setText(dataModel.getPrice().concat(" تومان"));
                    mRatingBar.setRating(dataModel.getRating_count());
                    mExpandTextView.setText(dataModel.getShort_description());

                    if (dataModel.getImages().size() > 0) {
                        mViewPager.setAdapter(new SliderAdapter(mActivity, dataModel.getImages(), null));
                    }
                    mIndicator.setViewPager(mViewPager);

                    if ("draft".equalsIgnoreCase(dataModel.getStatus()) || "private".equalsIgnoreCase(dataModel.getStatus())) {
                        mLayoutAddShopping.setEnabled(false);
                    }

                    if (dataModel.isFeatured()) {
                        mTvSpecial.setVisibility(View.VISIBLE);
                    }

                    if (TextUtils.isEmpty(dataModel.getDate_on_sale_to())) {
                        mCountdownView.setVisibility(View.GONE);
                    } else {
                        mCountdownView.setVisibility(View.VISIBLE);
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
                        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Tehran"));
                        long time = sdf.parse(dataModel.getDate_on_sale_to()).getTime();
                        time = time - System.currentTimeMillis();
                        if (time > 0) {
                            mTvPrice.setText(dataModel.getSale_price().concat(" تومان"));
                            mCountdownView.start(time);
                        } else {
                            mCountdownView.setVisibility(View.GONE);
                        }

                    }

                   /* if ("simple".equalsIgnoreCase(dataModel.getType())) {

                    } else if ("grouped".equalsIgnoreCase(dataModel.getType())) {

                    }else if ("external".equalsIgnoreCase(dataModel.getType())) {

                    } else */
                    if ("variable".equalsIgnoreCase(dataModel.getType())) {
                        webserviceGetVariations();
                        mLayoutVariable.setVisibility(View.VISIBLE);
                        LinearLayout layoutVariable = new LinearLayout(mActivity);
                        layoutVariable.setOrientation(LinearLayout.VERTICAL);

                        for (int i = 0; i < dataModel.getAttributes().size(); i++) {
                            View view = LayoutInflater.from(mActivity).inflate(R.layout.item_variable, layoutVariable, false);
                            TextView textView = (TextView) view.findViewById(R.id.Tv_Title);
                            Spinner spinner = (Spinner) view.findViewById(R.id.spinner_item);
                            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    parent.setTag(position);
                                    CalculatePrice();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                            textView.setText(dataModel.getAttributes().get(i).getName());

                            ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(mActivity, R.layout.spinner_title, R.id.lblSpn, dataModel.getAttributes().get(i).getOptions());
                            dataAdapter.setDropDownViewResource(R.layout.spinner_in_title);
                            spinner.setAdapter(dataAdapter);

                            layoutVariable.addView(view);
                        }
                        mLayoutVariable.addView(layoutVariable, new CardView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
            }

            @Override
            public void error(VolleyResponseModel callback) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Utils.Toast("error");
            }
        });
    }

    private void CalculatePrice() {
        if (modelsVariations.size() > 0) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    if (mLayoutVariable.getChildCount() == 1) {
                        List<String> selected = new ArrayList<>();
                        if (mLayoutVariable.getChildAt(0) instanceof LinearLayout) {
                            LinearLayout layoutVariable = (LinearLayout) mLayoutVariable.getChildAt(0);
                            for (int i = 0; i < layoutVariable.getChildCount(); i++) {
                                if (layoutVariable.getChildAt(i) instanceof LinearLayout) {
                                    LinearLayout layout = (LinearLayout) layoutVariable.getChildAt(i);
                                    for (int j = 0; j < layout.getChildCount(); j++) {
                                        if (layout.getChildAt(j) instanceof Spinner) {
                                            Spinner spinner = (Spinner) layout.getChildAt(j);
                                            if (spinner.getChildAt(0) instanceof RelativeLayout) {
                                                RelativeLayout relativeLayout = (RelativeLayout) spinner.getChildAt(0);
                                                if (relativeLayout.getChildAt(0) instanceof TextView) {
                                                    TextView textView = (TextView) relativeLayout.getChildAt(0);
                                                    selected.add(textView.getText().toString());
                                                }
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                            for (int i = 0; i < modelsVariations.size(); i++) {
                                final ModuleDataModel model = modelsVariations.get(i);
                                for (int j = 0; j < model.getAttributes().size(); j++) {
                                    Attributes attributes = model.getAttributes().get(j);
                                    if (attributes.getOption().equalsIgnoreCase(selected.get(j))) {
                                        position_Variation_selected = i;
                                    } else {
                                        position_Variation_selected = -1;
                                        break;
                                    }
                                }
                                if (position_Variation_selected != -1) {
                                    mApplication.applicationHandler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            dataModel.setPrice(model.getPrice());
                                            dataModel.setRegular_price(model.getRegular_price());
                                            mTvPrice.setText(model.getPrice().concat(" تومان"));
                                        }
                                    });
                                    break;
                                }
                            }
                        }
                    }
                }
            };
            new Thread(runnable).start();
        }
    }

    private void webserviceGetVariations() {

        String URL = new RequestQuery(RequestQuery.UrlOAuthType.GET_PRODUCTS)
                .addRoute(String.valueOf(ID))
                .addRoute("variations")
                .add("page", String.valueOf(pageVariations)).GetUrl();

        RestRequest.getInstance().AsynchronousServiceCall(URL, new callbackService() {
            @Override
            public void success(VolleyResponseModel callback) {
                try {
                    JSONArray jsonArray = new JSONArray(callback.getResponse());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        modelsVariations.add(Utils.serializeJsonProduct(jsonArray.getJSONObject(i)));
                    }
                    if (jsonArray.length() == 10) {
                        pageVariations++;
                        webserviceGetVariations();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.Iv_Back:
                onBackPressed();
                break;
            case R.id.iv_shopping_cart:
                Utils.startShoppingBagActivity(mActivity);
                break;
            case R.id.Iv_Share:
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, dataModel.getName());
                i.putExtra(Intent.EXTRA_TEXT, dataModel.getPermalink());
                startActivity(Intent.createChooser(i, "اشتراک گذاری :"));
                break;
            case R.id.Layout_Add_Shopping:
                if ("publish".equalsIgnoreCase(dataModel.getStatus())) {
                    ShoppingBag model = new ShoppingBag();
                    model.setId(dataModel.getId());
                    model.setImage(dataModel.getImages().get(0));
                    model.setTitle(dataModel.getName());
                    model.setPrice(dataModel.getPrice());
                    model.setRegular_price(dataModel.getRegular_price());
                    model.setOn_sale(dataModel.isOn_sale());
                    model.setAttribute(position_Variation_selected >= 0 ? modelsVariations.get(position_Variation_selected) : null);
                    Utils.dialogShoppingBag(mActivity, model);
                } else if ("pending".equalsIgnoreCase(dataModel.getStatus())) {
                    Utils.Toast("به زودی");
                }
                break;
            case R.id.Layout_Comment:
                startActivity(new Intent(mActivity, UserCommentsActivity.class).putExtras(getIntent()));
                break;
            case R.id.Layout_Details:
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        final LinearLayout tableLayout = new LinearLayout(mActivity);
                        tableLayout.setOrientation(LinearLayout.VERTICAL);
                        LinearLayout.LayoutParams params_Row = new LinearLayout.LayoutParams(CardView.LayoutParams.MATCH_PARENT, CardView.LayoutParams.WRAP_CONTENT);
                        tableLayout.addView(CreateRowTable("عنوان", "توضیحات", true), params_Row);
                        for (int j = 0; j < dataModel.getAttributes().size(); j++) {
                            String text = "";
                            List<String> options = dataModel.getAttributes().get(j).getOptions();
                            int count = options.size();
                            for (int k = 0; k < count; k++) {
                                if (k == 0) {
                                    text += options.get(k);
                                } else {
                                    text += " , " + options.get(k);
                                }
                            }
                            tableLayout.addView(CreateRowTable(dataModel.getAttributes().get(j).getName(), text, false), params_Row);
                        }

                        if (!Utils.isEmpty(dataModel.getSku())) {
                            tableLayout.addView(CreateRowTable("شناسه منحصر به فرد", dataModel.getSku(), false), params_Row);
                        }

                        tableLayout.addView(CreateRowTable("تعداد فروش", String.valueOf(dataModel.getTotal_sales()), false), params_Row);

                        if (!Utils.isEmpty(dataModel.getWeight())) {
                            tableLayout.addView(CreateRowTable("وزن", dataModel.getWeight(), false), params_Row);
                        }

                        if (dataModel.getDimensions() != null && !TextUtils.isEmpty(dataModel.getDimensions().getHeight())) {
                            String text = dataModel.getDimensions().getWidth() + " * " + dataModel.getDimensions().getLength() + " * " + dataModel.getDimensions().getHeight();
                            tableLayout.addView(CreateRowTable("ابعاد", text, false), params_Row);
                        }

                        final CardView cardview = new CardView(mActivity);

                        ViewGroup.LayoutParams layoutparams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                        cardview.setLayoutParams(layoutparams);

                        cardview.setRadius(15);

                        cardview.setPadding(25, 25, 25, 25);

                        cardview.setMaxCardElevation(30);

                        cardview.addView(tableLayout);

                        mApplication.applicationHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                Utils.CustomDialog(mActivity).customView(cardview, false).show();
                            }
                        });
                    }
                };
                new Thread(runnable).start();
                break;
        }

    }

    @SuppressLint("RtlHardcoded")
    private View CreateRowTable(String title1, String title2, boolean isBold) {
        LinearLayout.LayoutParams params_Tv = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1);
        params_Tv.gravity = Gravity.CENTER;

        LinearLayout tr = new LinearLayout(mActivity);
        tr.setOrientation(LinearLayout.HORIZONTAL);
        tr.setBackground(Utils.getDrawable(R.drawable.cell_shape));

        TextView tv0 = new TextView(mActivity), tv1 = new TextView(mActivity);

        tv0.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
        tv1.setGravity(Gravity.CENTER);

//        tv0.setBackground(Utils.getDrawable(R.drawable.cell_shape));
//        tv1.setBackground(Utils.getDrawable(R.drawable.cell_shape));

        if (isBold) {
            tv0.setTypeface(mApplication.IRANSansBold, Typeface.BOLD);
            tv1.setTypeface(mApplication.IRANSansBold, Typeface.BOLD);
        }

        tv0.setPadding(Utils.dp(5), Utils.dp(10), Utils.dp(5), Utils.dp(10));
        tv1.setPadding(Utils.dp(5), Utils.dp(10), Utils.dp(5), Utils.dp(10));

        tv0.setText(title1);
        tv1.setText(title2);
        tr.addView(tv1, params_Tv);
        tr.addView(tv0, params_Tv);

        return tr;
    }
}
