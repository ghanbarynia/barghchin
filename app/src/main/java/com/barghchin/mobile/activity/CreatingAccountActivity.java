package com.barghchin.mobile.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.barghchin.mobile.ModelData.UserProfileModel;
import com.barghchin.mobile.ModelData.VolleyResponseModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.utility.MySharedPreferences;
import com.barghchin.mobile.utility.RequestQuery;
import com.barghchin.mobile.utility.RestRequest;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.mApplication;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CreatingAccountActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mETEmail, mETName, mETPassword;
    private MaterialDialog pDialog;
    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creating_account);
        initView();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initView() {
        mActivity = this;
        findViewById(R.id.TopToolbar).setBackgroundColor(Utils.getAppThemeColor());
        TextView mTvTitle = (TextView) findViewById(R.id.Tv_Title);
        mTvTitle.setText(getString(R.string.Creating_account));

        mETEmail = (EditText) findViewById(R.id.ET_email);
        mETName = (EditText) findViewById(R.id.ET_username);
        mETPassword = (EditText) findViewById(R.id.ET_password);
        TextView mTVCreatingAccount = (TextView) findViewById(R.id.TV_CreatingAccount);

        mETEmail.setTypeface(mApplication.IRANSans);
        mETName.setTypeface(mApplication.IRANSans);
        mETPassword.setTypeface(mApplication.IRANSans);
        mTVCreatingAccount.setTypeface(mApplication.IRANSans);

        findViewById(R.id.Layout_CreatingAccount).setOnClickListener(this);
        findViewById(R.id.Iv_Back).setOnClickListener(this);
        findViewById(R.id.ic_shopping_cart).setOnClickListener(this);
        findViewById(R.id.ic_menu_search).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.Layout_CreatingAccount:
                if (TextUtils.isEmpty(mETName.getText().toString())) {
                    mETName.setError("لطفا نام کاربری را وارد کنید.");
                    break;
                } else {
                    mETName.setError(null);
                }
                if (TextUtils.isEmpty(mETEmail.getText().toString())) {
                    mETEmail.setError("لطفا ایمیل را وارد کنید.");
                    break;
                } else {
                    mETEmail.setError(null);
                }
                if (TextUtils.isEmpty(mETPassword.getText().toString())) {
                    mETPassword.setError("لطفا رمز عبور را وارد کنید.");
                    break;
                } else {
                    mETPassword.setError(null);
                }
                if (pDialog == null) {
                    pDialog = Utils.CustomDialog(mActivity).content(R.string.please_wait).progress(true, 0).show();
                } else {
                    pDialog.show();
                }


                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        Map<String, String> params = new HashMap<>();
                        params.put("username", mETName.getText().toString());
                        params.put("email", mETEmail.getText().toString());
                        params.put("password", mETPassword.getText().toString());
                        params.put("display_name", mETEmail.getText().toString());

                        String URL = new RequestQuery(RequestQuery.UrlOAuthType.POST_REGISTER)
                                .add("username", mETName.getText().toString())
                                .add("email", mETEmail.getText().toString())
                                .add("display_name", mETEmail.getText().toString())
                                .add("password", mETPassword.getText().toString()).GetUrl();

                        VolleyResponseModel responseModel = RestRequest.getInstance().SynchronousServiceCall(URL, Request.Method.POST, params);

                        if (!TextUtils.isEmpty(responseModel.getResponse())) {
                            try {
                                JSONObject jsonObject = new JSONObject(responseModel.getResponse());
                                if (jsonObject.has("status") && !jsonObject.isNull("status") && "ok".equalsIgnoreCase(jsonObject.getString("status"))) {
                                    UserProfileModel profileModel = new UserProfileModel();

                                    if (jsonObject.has("cookie") && !jsonObject.isNull("cookie")) {
                                        profileModel.setCookie(jsonObject.getString("cookie"));
                                    }

                                    if (jsonObject.has("user_id") && !jsonObject.isNull("user_id")) {
                                        profileModel.setId(jsonObject.getInt("user_id"));
                                    }

                                    profileModel.setEmail(mETEmail.getText().toString());
                                    MySharedPreferences.getInstance().setPreference(MySharedPreferences.USER_PROFILE, new Gson().toJson(profileModel));

                                    mApplication.applicationHandler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            onBackPressed();
                                        }
                                    });
                                } else {
                                    mApplication.applicationHandler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (pDialog != null && pDialog.isShowing()) {
                                                pDialog.dismiss();
                                            }
                                            Utils.Toast("این کاربر قبلا با این مشخصات ثبت نامشده");
                                        }
                                    });
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else {
                            mApplication.applicationHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (pDialog != null && pDialog.isShowing()) {
                                        pDialog.dismiss();
                                    }
                                    Utils.Toast("خطا در برقراری ارتباط با سرور لطفا بعدا تلاش کنید");
                                }
                            });
                        }
                    }
                };

                new Thread(runnable).start();

                break;

            case R.id.ic_shopping_cart:
                Utils.startShoppingBagActivity(mActivity);
                break;

            case R.id.ic_menu_search:
                Utils.startSearchActivity(mActivity);
                break;

            case R.id.Iv_Back:
                onBackPressed();
                break;
        }
    }
}
