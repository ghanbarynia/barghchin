package com.barghchin.mobile.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.barghchin.mobile.ModelData.OrdersModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.utility.RequestQuery;
import com.barghchin.mobile.utility.RestRequest;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.mApplication;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ShoppingHistoryDetailActivity extends AppCompatActivity {

    private TextView mTvTotalCard, mTvStatus;
    private ListView mListView;
    private String total = "", status = "";
    private List<OrdersModel> ordersModels = new ArrayList<>();
    private ShoppingHistoryDetailActivity mActivity;
    private MaterialDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_history_detail);
        initView();
        loadData();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void loadData() {
        if (pDialog == null) {
            pDialog = Utils.CustomDialog(mActivity).content(R.string.please_wait).progress(true, 0).show();
        } else {
            pDialog.show();
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                String URL = new RequestQuery(RequestQuery.UrlOAuthType.GET_ORDERS)
                        .addRoute(getIntent().getStringExtra("ID"))
                        .GetUrl();

                String response = RestRequest.getInstance().SynchronousServiceCall(URL, Request.Method.GET, null).getResponse();
                if (TextUtils.isEmpty(response)) {
                    Utils.Toast("خطا در بر قراری ارتباط با سرور ، لطفا بعدا امتحان کنید");
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has("total") && !jsonObject.isNull("total")) {
                            total = jsonObject.getString("total");
                        }
                        if (jsonObject.has("status") && !jsonObject.isNull("status")) {
                            status = Utils.detectStatus(jsonObject.getString("status"));
                        }
                        if (jsonObject.has("line_items") && !jsonObject.isNull("line_items")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("line_items");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                OrdersModel model = new OrdersModel();
                                JSONObject json = jsonArray.getJSONObject(i);
                                if (json.has("id") && !json.isNull("id")) {
                                    model.setId(json.getInt("id"));
                                }
                                if (json.has("name") && !json.isNull("name")) {
                                    model.setName(json.getString("name"));
                                }
                                if (json.has("quantity") && !json.isNull("quantity")) {
                                    model.setQuantity(json.getInt("quantity"));
                                }
                                if (json.has("total") && !json.isNull("total")) {
                                    model.setTotal(json.getString("total"));
                                }
                                if (json.has("subtotal") && !json.isNull("subtotal")) {
                                    model.setSubtotal(json.getString("subtotal"));
                                }
                                ordersModels.add(model);
                            }
                        }
                        mApplication.applicationHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                mTvTotalCard.setText(total);
                                mTvStatus.setText(status);
                                mListView.setAdapter(new CustomAdapter());
                                if (pDialog != null && pDialog.isShowing()) {
                                    pDialog.dismiss();
                                }
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private void initView() {
        mActivity = this;
        findViewById(R.id.Iv_Back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        findViewById(R.id.ic_shopping_cart).setVisibility(View.GONE);
        findViewById(R.id.ic_menu_search).setVisibility(View.GONE);
        findViewById(R.id.TopToolbar).setBackgroundColor(Utils.getAppThemeColor());
        TextView mTvTitle = (TextView) findViewById(R.id.Tv_Title);
        mTvTitle.setText(getIntent().getStringExtra("title"));

        mTvTotalCard = (TextView) findViewById(R.id.tv_total_card);
        mTvStatus = (TextView) findViewById(R.id.tv_status);
        mListView = (ListView) findViewById(R.id.list_view);
    }

    private class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            if (ordersModels == null) {
                return 0;
            }
            return ordersModels.size();
        }

        @Override
        public Object getItem(int position) {
            return ordersModels.get(position);
        }

        @Override
        public long getItemId(int position) {
            return ordersModels.get(position).hashCode();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                convertView = LayoutInflater.from(mActivity).inflate(R.layout.item_shopping_history_detail, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.mName = (TextView) convertView.findViewById(R.id.Tv_name);
                viewHolder.mQuantity = (TextView) convertView.findViewById(R.id.Tv_quantity);
                viewHolder.mTotal = (TextView) convertView.findViewById(R.id.Tv_total);
                viewHolder.mSubtotal = (TextView) convertView.findViewById(R.id.Tv_subtotal);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.mName.setText(ordersModels.get(position).getName());
            viewHolder.mQuantity.setText(String.valueOf(ordersModels.get(position).getQuantity()));
            viewHolder.mTotal.setText(ordersModels.get(position).getTotal().concat(" ريال"));
            viewHolder.mSubtotal.setText(ordersModels.get(position).getSubtotal().concat(" ريال"));

            return convertView;
        }
    }

    private class ViewHolder {
        TextView mName, mQuantity, mTotal, mSubtotal;
    }
}
