package com.barghchin.mobile.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.barghchin.mobile.Adapter.CustomAdapter_Favorites;
import com.barghchin.mobile.R;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.mApplication;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FavoritesActivity extends AppCompatActivity implements View.OnClickListener {

    private FavoritesActivity mActivity;
    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_category);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LoadData();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ic_shopping_cart:
                Utils.startShoppingBagActivity(mActivity);
                break;
            case R.id.ic_menu_search:
                Utils.startSearchActivity(mActivity);
                break;
            case R.id.Iv_Back:
                onBackPressed();
                break;
        }
    }

    private void initView() {
        mActivity = this;
        findViewById(R.id.Iv_Back).setOnClickListener(this);
        findViewById(R.id.ic_shopping_cart).setOnClickListener(this);
        findViewById(R.id.ic_menu_search).setOnClickListener(this);
        TextView mTvTitle = (TextView) findViewById(R.id.Tv_Title);
        findViewById(R.id.TopToolbar).setBackgroundColor(Utils.getAppThemeColor());
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_ProductCategoryActivity);
        mTvTitle.setTypeface(mApplication.IRANSansBold);
        mTvTitle.setText("محصولات مورد علاقه");

        final LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setAutoMeasureEnabled(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setLayoutManager(layoutManager);
    }

    private void LoadData() {
        mRecyclerView.setAdapter(new CustomAdapter_Favorites(mActivity, mApplication.db.selectGalleryPack()));
    }
}
