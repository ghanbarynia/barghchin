package com.barghchin.mobile.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.barghchin.mobile.Adapter.OrdersAdapter;
import com.barghchin.mobile.ModelData.OrdersModel;
import com.barghchin.mobile.ModelData.VolleyResponseModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.utility.RequestQuery;
import com.barghchin.mobile.utility.RestRequest;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.callbackService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ShoppingHistoryActivity extends AppCompatActivity implements View.OnClickListener {

    private MaterialDialog pDialog;
    private ShoppingHistoryActivity mActivity;
    private List<OrdersModel> mData = new ArrayList<>();
    private OrdersAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_history);
        initView();
        LoadData();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ic_shopping_cart:
                Utils.startShoppingBagActivity(mActivity);
                break;
            case R.id.ic_menu_search:
                startActivity(new Intent(mActivity, SearchActivity.class));
                break;
            case R.id.Iv_Back:
                onBackPressed();
                break;
        }
    }

    @SuppressLint("InflateParams")
    private void initView() {
        mActivity = this;
        findViewById(R.id.Iv_Back).setOnClickListener(this);
        findViewById(R.id.ic_shopping_cart).setOnClickListener(this);
        findViewById(R.id.ic_menu_search).setOnClickListener(this);
        findViewById(R.id.TopToolbar).setBackgroundColor(Utils.getAppThemeColor());
        TextView mTvTitle = (TextView) findViewById(R.id.Tv_Title);
        mTvTitle.setText("سفارشات");

        ListView mListView = (ListView) findViewById(R.id.listView_ShoppingHistoryActivity);
        mListView.setAdapter(mAdapter = new OrdersAdapter(mActivity, R.layout.item_categories_list, mData));
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(mActivity, ShoppingHistoryDetailActivity.class).putExtra("ID", String.valueOf(mData.get(position).getId())).putExtra("title", view.getTag().toString()));
            }
        });
    }

    private void LoadData() {
        if (pDialog == null) {
            pDialog = Utils.CustomDialog(mActivity).content(R.string.please_wait).progress(true, 0).show();
        } else {
            pDialog.show();
        }

        String URL = new RequestQuery(RequestQuery.UrlOAuthType.GET_ORDERS)
                .add("customer_id", String.valueOf(getIntent().getIntExtra("ID", -1)))
                .GetUrl();

        RestRequest.getInstance().AsynchronousServiceCall(URL, new callbackService() {
            @Override
            public void success(VolleyResponseModel callback) {
                try {
                    JSONArray jsonArray = new JSONArray(callback.getResponse());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        OrdersModel ordersModel = new OrdersModel();
                        mData.add(ordersModel);
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        if (jsonObject.has("id") && !jsonObject.isNull("id")) {
                            ordersModel.setId(jsonObject.getInt("id"));
                        }
                        if (jsonObject.has("status") && !jsonObject.isNull("status")) {
                            ordersModel.setStatus(Utils.detectStatus(jsonObject.getString("status")));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mAdapter.notifyDataSetChanged();
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
            }

            @Override
            public void error(VolleyResponseModel callback) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Utils.Toast("error");
            }
        });
    }
}
