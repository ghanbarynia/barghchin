package com.barghchin.mobile.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.barghchin.mobile.ModelData.UserProfileModel;
import com.barghchin.mobile.ModelData.VolleyResponseModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.utility.MySharedPreferences;
import com.barghchin.mobile.utility.RequestQuery;
import com.barghchin.mobile.utility.RestRequest;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.mApplication;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LogInActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mET_Phone, mET_key;
    private Activity mActivity;
    private MaterialDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        initView();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initView() {
        mActivity = this;
        findViewById(R.id.TopToolbar).setBackgroundColor(Utils.getAppThemeColor());
        TextView mTvTitle = (TextView) findViewById(R.id.Tv_Title);
        mTvTitle.setText(getString(R.string.Login_account));

        View mLayoutCreatingAccount = findViewById(R.id.Layout_CreatingAccount);
        View mLayoutLoginAccount = findViewById(R.id.Layout_LoginAccount);
        mET_Phone = (EditText) findViewById(R.id.ET_Phone);
        mET_key = (EditText) findViewById(R.id.ET_key);
        TextView mTV_LoginAccount = (TextView) findViewById(R.id.TV_Login_account);
        TextView mTV_ForgetPassword = (TextView) findViewById(R.id.TV_ForgetPassword);
        TextView mTV_CreatingAccount = (TextView) findViewById(R.id.TV_CreatingAccount);

        mLayoutCreatingAccount.setOnClickListener(this);
        mLayoutLoginAccount.setOnClickListener(this);
        mTV_ForgetPassword.setOnClickListener(this);
        findViewById(R.id.Iv_Back).setOnClickListener(this);
        findViewById(R.id.ic_shopping_cart).setOnClickListener(this);
        findViewById(R.id.ic_menu_search).setOnClickListener(this);

        mET_Phone.setTypeface(mApplication.IRANSans);
        mET_key.setTypeface(mApplication.IRANSans);
        mTV_LoginAccount.setTypeface(mApplication.IRANSans);
        mTV_ForgetPassword.setTypeface(mApplication.IRANSans);
        mTV_CreatingAccount.setTypeface(mApplication.IRANSans);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.Layout_CreatingAccount:
                startActivity(new Intent(this, CreatingAccountActivity.class));
                finish();
                break;
            case R.id.TV_ForgetPassword:
                DialogForgetPassword();
                break;

            case R.id.Layout_LoginAccount:
                if (TextUtils.isEmpty(mET_Phone.getText().toString())) {
                    mET_Phone.setError("لطفا ایمیل یا نام کاربری را وارد کنید.");
                    break;
                } else {
                    mET_Phone.setError(null);
                }
                if (TextUtils.isEmpty(mET_key.getText().toString())) {
                    mET_key.setError("لطفا رمز عبور را وارد کنید.");
                    break;
                } else {
                    mET_key.setError(null);
                }
                if (pDialog == null) {
                    pDialog = Utils.CustomDialog(mActivity).content(R.string.please_wait).progress(true, 0).show();
                } else {
                    pDialog.show();
                }

                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        Map<String, String> params = new HashMap<>();
                        params.put("username", mET_Phone.getText().toString());
                        params.put("password", mET_key.getText().toString());

                        String URL = new RequestQuery(RequestQuery.UrlOAuthType.POST_LOGIN).
                                add("username", mET_Phone.getText().toString())
                                .add("password", mET_key.getText().toString()).GetUrl();

                        VolleyResponseModel responseModel = RestRequest.getInstance().SynchronousServiceCall(URL, Request.Method.POST, params);
                        if (!TextUtils.isEmpty(responseModel.getResponse())) {
                            try {
                                JSONObject jsonObject = new JSONObject(responseModel.getResponse());
                                if (jsonObject.has("status") && !jsonObject.isNull("status") && "ok".equalsIgnoreCase(jsonObject.getString("status"))) {
                                    UserProfileModel profileModel = new UserProfileModel();

                                    if (jsonObject.has("cookie") && !jsonObject.isNull("cookie")) {
                                        profileModel.setCookie(jsonObject.getString("cookie"));
                                    }

                                    if (jsonObject.has("user") && !jsonObject.isNull("user")) {
                                        JSONObject user = jsonObject.getJSONObject("user");

                                        if (user.has("id") && !user.isNull("id")) {
                                            profileModel.setId(user.getInt("id"));
                                        }

                                        if (user.has("username") && !user.isNull("username")) {
                                            profileModel.setUsername(user.getString("username"));
                                        }

                                        if (user.has("nicename") && !user.isNull("nicename")) {
                                            profileModel.setNicename(user.getString("nicename"));
                                        }

                                        if (user.has("email") && !user.isNull("email")) {
                                            profileModel.setEmail(user.getString("email"));
                                        }

                                        if (user.has("url") && !user.isNull("url")) {
                                            profileModel.setUrl(user.getString("url"));
                                        }

                                        if (user.has("registered") && !user.isNull("registered")) {
                                            profileModel.setRegistered(user.getString("registered"));
                                        }

                                        if (user.has("displayname") && !user.isNull("displayname")) {
                                            profileModel.setDisplayname(user.getString("displayname"));
                                        }

                                        if (user.has("firstname") && !user.isNull("firstname")) {
                                            profileModel.setFirst_name(user.getString("firstname"));
                                        }

                                        if (user.has("lastname") && !user.isNull("lastname")) {
                                            profileModel.setLast_name(user.getString("lastname"));
                                        }

                                        if (user.has("nickname") && !user.isNull("nickname")) {
                                            profileModel.setNickname(user.getString("nickname"));
                                        }

                                        if (user.has("description") && !user.isNull("description")) {
                                            profileModel.setDescription(Utils.fromHtml(user.getString("description")));
                                        }

                                        if (user.has("capabilities") && !user.isNull("capabilities")) {
                                            profileModel.setCapabilities(user.getString("capabilities"));
                                        }

                                        if (user.has("avatar") && !user.isNull("avatar")) {
                                            profileModel.setAvatar(user.getString("avatar"));
                                        }
                                    }

                                    MySharedPreferences.getInstance().setPreference(MySharedPreferences.USER_PROFILE, new Gson().toJson(profileModel));
                                    mApplication.applicationHandler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            onBackPressed();
                                        }
                                    });
                                } else {
                                    mApplication.applicationHandler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (pDialog != null && pDialog.isShowing()) {
                                                pDialog.dismiss();
                                            }
                                            Utils.Toast("نام کاربری یا رمز عبور اشتباه است");
                                        }
                                    });
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                };

                new Thread(runnable).start();

                break;

            case R.id.ic_shopping_cart:
                Utils.startShoppingBagActivity(mActivity);
                break;

            case R.id.ic_menu_search:
                Utils.startSearchActivity(mActivity);
                break;

            case R.id.Iv_Back:
                onBackPressed();
                break;
        }
    }

    private void DialogForgetPassword() {
        /*final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_forget_password);

        TextView mTvTitle = (TextView) dialog.findViewById(R.id.TV_Title);
        TextView mTvContent = (TextView) dialog.findViewById(R.id.TV_Content);
        EditText mEtPhoneNumber = (EditText) dialog.findViewById(R.id.ET_PhoneNumber);
        TextView mTvOk = (TextView) dialog.findViewById(R.id.TV_OK);
        mTvTitle.setTypeface(mApplication.IRANSans);
        mTvContent.setTypeface(mApplication.IRANSans);
        mEtPhoneNumber.setTypeface(mApplication.IRANSans);
        mTvOk.setTypeface(mApplication.IRANSans);

        mTvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = (int) (mApplication.mPoint.x * 0.9);
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);*/
        Utils.CustomDialog(mActivity)
                .customView(R.layout.dialog_forget_password, false)
                .show();
    }
}
