package com.barghchin.mobile.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.barghchin.mobile.Adapter.MainActivityAdapter;
import com.barghchin.mobile.ModelData.MainActivityModel;
import com.barghchin.mobile.ModelData.ModuleDataModel;
import com.barghchin.mobile.ModelData.VolleyResponseModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.utility.RequestQuery;
import com.barghchin.mobile.utility.RestRequest;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.callbackService;
import com.barghchin.mobile.utility.mApplication;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProductCategoryActivity extends AppCompatActivity implements View.OnClickListener {

    private MaterialDialog pDialog;
    private TextView mTvTitle;
    private ProductCategoryActivity mActivity;
    private List<MainActivityModel> mData = new ArrayList<>();
    private MainActivityAdapter mAdapter;
    private String CategoryId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_category);
        initView();
        LoadData();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ic_shopping_cart:
                Utils.startShoppingBagActivity(mActivity);
                break;
            case R.id.ic_menu_search:
                Utils.startSearchActivity(mActivity);
                break;
            case R.id.Iv_Back:
                onBackPressed();
                break;
        }
    }

    private void initView() {
        mActivity = this;
        findViewById(R.id.Iv_Back).setOnClickListener(this);
        findViewById(R.id.ic_shopping_cart).setOnClickListener(this);
        findViewById(R.id.ic_menu_search).setOnClickListener(this);
        mTvTitle = (TextView) findViewById(R.id.Tv_Title);
        findViewById(R.id.TopToolbar).setBackgroundColor(Utils.getAppThemeColor());
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_ProductCategoryActivity);

        mTvTitle.setTypeface(mApplication.IRANSansBold);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setAutoMeasureEnabled(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setLayoutManager(layoutManager);

        mRecyclerView.setAdapter(mAdapter = new MainActivityAdapter(mActivity, mData));
    }

    private void LoadData() {
        Intent intent = getIntent();
        CategoryId = String.valueOf(intent.getIntExtra("CategoryId", -1));
        mTvTitle.setText(intent.getStringExtra("CategoryTitle"));
        callWebserviceChildCategory();
        getProducts();
    }


    private void callWebserviceChildCategory() {
        MainActivityModel category_Model1 = new MainActivityModel();
        mData.add(category_Model1);
        category_Model1.setType(MainActivityAdapter.VIEW_TYPES.Category);

        String URL = new RequestQuery(RequestQuery.UrlOAuthType.GET_PRODUCTS)
                .addRoute("categories")
                .add("parent", CategoryId)
                .add("page", "1")
                .GetUrl();

        RestRequest.getInstance().AsynchronousServiceCall(URL, new callbackService() {
            @Override
            public void success(VolleyResponseModel callback) {
                try {
                    List<ModuleDataModel> module_data = new ArrayList<>();
                    mData.get(0).setModule_data(module_data);

                    JSONArray jsonArray = new JSONArray(callback.getResponse());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        ModuleDataModel dataModel = new ModuleDataModel();

                        if (jsonObject.has("id") && !jsonObject.isNull("id")) {
                            dataModel.setId(jsonObject.getInt("id"));
                        }

                        if (jsonObject.has("name") && !jsonObject.isNull("name")) {
                            dataModel.setTitle(jsonObject.getString("name"));
                        }
                        module_data.add(dataModel);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mAdapter.notifyItemChanged(0);
            }

            @Override
            public void error(VolleyResponseModel callback) {
                Utils.Toast("error");
            }
        });
    }

    private void getProducts() {
        MainActivityModel category_Model2 = new MainActivityModel();
        mData.add(category_Model2);
        category_Model2.setType(MainActivityAdapter.VIEW_TYPES.Category_Product_Linear);
        ShowDialog();

        String URL = new RequestQuery(RequestQuery.UrlOAuthType.GET_PRODUCTS_NEW)
                .add("category", CategoryId).GetUrl();

        RestRequest.getInstance().AsynchronousServiceCall(URL, new callbackService() {
            @Override
            public void success(VolleyResponseModel callback) {
                try {
                    List<ModuleDataModel> module_data = new ArrayList<>();
                    mData.get(1).setModule_data(module_data);


                    JSONArray jsonArray = new JSONArray(callback.getResponse());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        ModuleDataModel dataModel = new ModuleDataModel();

                        if (jsonObject.has("id") && !jsonObject.isNull("id")) {
                            dataModel.setId(jsonObject.getInt("id"));
                        }

                        if (jsonObject.has("name") && !jsonObject.isNull("name")) {
                            dataModel.setTitle(jsonObject.getString("name"));
                        }

                        if (jsonObject.has("price") && !jsonObject.isNull("price")) {
                            dataModel.setPrice(jsonObject.getString("price"));
                        }

                        if (jsonObject.has("short_description") && !jsonObject.isNull("short_description")) {
                            dataModel.setShort_description(Utils.fromHtml(jsonObject.getString("short_description")));
                        }

                        if (jsonObject.has("images") && !jsonObject.isNull("images")) {
                            JSONArray images = jsonObject.getJSONArray("images");
                            if (images.getJSONObject(0).has("src") && !images.getJSONObject(0).isNull("src")) {
                                dataModel.setImage(images.getJSONObject(0).getString("src"));
                            }
                        }
                        module_data.add(dataModel);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mAdapter.notifyItemChanged(1);
                DismissDialog();
            }

            @Override
            public void error(VolleyResponseModel callback) {
                DismissDialog();
            }
        });
    }

    private void ShowDialog() {
        if (pDialog == null) {
            pDialog = Utils.CustomDialog(mActivity).content(R.string.please_wait).progress(true, 0).show();
        } else {
            pDialog.show();
        }
    }

    private void DismissDialog() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }
}
