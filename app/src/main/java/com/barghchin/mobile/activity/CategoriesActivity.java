package com.barghchin.mobile.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.barghchin.mobile.Adapter.CategoriesAdapter;
import com.barghchin.mobile.ModelData.CategoriesModel;
import com.barghchin.mobile.ModelData.VolleyResponseModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.utility.RequestQuery;
import com.barghchin.mobile.utility.RestRequest;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.callbackService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CategoriesActivity extends AppCompatActivity implements View.OnClickListener {

    private ListView mListView;
    private MaterialDialog pDialog;
    private CategoriesActivity mActivity;
    private List<CategoriesModel> mData = new ArrayList<>();
    private boolean flag = false;
    private int page = 1;
    private View mFooterView;
    private CategoriesAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        initView();
        LoadData();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ic_shopping_cart:
                Utils.startShoppingBagActivity(mActivity);
                break;
            case R.id.ic_menu_search:
                Utils.startSearchActivity(mActivity);
                break;
            case R.id.Iv_Back:
                onBackPressed();
                break;
        }
    }

    @SuppressLint("InflateParams")
    private void initView() {
        mActivity = this;
        findViewById(R.id.Iv_Back).setOnClickListener(this);
        findViewById(R.id.ic_shopping_cart).setOnClickListener(this);
        findViewById(R.id.ic_menu_search).setOnClickListener(this);
        findViewById(R.id.TopToolbar).setBackgroundColor(Utils.getAppThemeColor());
        TextView mTvTitle = (TextView) findViewById(R.id.Tv_Title);
        mTvTitle.setText("دسته بندی");

        mListView = (ListView) findViewById(R.id.listView_CategoriesActivity);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(mActivity, ProductCategoryActivity.class);
                intent.putExtra("CategoryId", String.valueOf(mData.get(i).getId()));
                intent.putExtra("CategoryTitle", mData.get(i).getName());
                mActivity.startActivity(intent);
            }
        });

        mFooterView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_layout, null, false);
        ProgressBar mProgressBar = (ProgressBar) mFooterView.findViewById(R.id.progressBar);
        mProgressBar.getIndeterminateDrawable().setColorFilter(Utils.getAppThemeColor(), PorterDuff.Mode.MULTIPLY);
        mListView.addFooterView(mFooterView);

        mListView.setAdapter(mAdapter = new CategoriesAdapter(mActivity, R.layout.item_categories_list, mData));

        mListView.removeFooterView(mFooterView);

        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if ((visibleItemCount == (totalItemCount - firstVisibleItem)) && flag && mData.size() > 0) {
                    if (mListView.getFooterViewsCount() == 0) {
                        mListView.addFooterView(mFooterView);
                    }

                    String URL = new RequestQuery(RequestQuery.UrlOAuthType.GET_PRODUCTS)
                            .addRoute("categories")
                            .add("parent", "0")
                            .add("page", String.valueOf(page))
                            .GetUrl();

                    //getString(R.string.urlCategorys, 1, page)
                    RestRequest.getInstance().AsynchronousServiceCall(URL, new callbackService() {
                        @Override
                        public void success(VolleyResponseModel callback) {
                            try {
                                JSONArray jsonArray = new JSONArray(callback.getResponse());
                                if (jsonArray.length() == 10) {
                                    flag = true;
                                    page++;
                                } else {
                                    flag = false;
                                }
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    CategoriesModel commentModel = new CategoriesModel();
                                    mData.add(commentModel);
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    if (jsonObject.has("id") && !jsonObject.isNull("id")) {
                                        commentModel.setId(jsonObject.getInt("id"));
                                    }
                                    if (jsonObject.has("name") && !jsonObject.isNull("name")) {
                                        commentModel.setName(jsonObject.getString("name"));
                                    }
                                    if (jsonObject.has("image") && !jsonObject.isNull("image")) {
                                        Object image = jsonObject.get("image");
                                        if (image instanceof JSONObject) {
                                            if (((JSONObject) image).has("src") && !((JSONObject) image).isNull("src")) {
                                                commentModel.setImage(((JSONObject) image).getString("src"));
                                            }
                                        } else if (image instanceof String) {
                                            commentModel.setImage((String) image);
                                        }
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            mAdapter.notifyDataSetChanged();
                            if (mListView.getFooterViewsCount() == 1) {
                                mListView.removeFooterView(mFooterView);
                            }
                        }

                        @Override
                        public void error(VolleyResponseModel callback) {
                            if (mListView.getFooterViewsCount() == 1) {
                                mListView.removeFooterView(mFooterView);
                            }
                            Utils.Toast("error");
                        }
                    });
                }
            }
        });
    }

    private void LoadData() {
        if (pDialog == null) {
            pDialog = Utils.CustomDialog(mActivity).content(R.string.please_wait).progress(true, 0).show();
        } else {
            pDialog.show();
        }

        String URL = new RequestQuery(RequestQuery.UrlOAuthType.GET_PRODUCTS)
                .addRoute("categories")
                .add("parent", "0")
                .GetUrl();

        RestRequest.getInstance().AsynchronousServiceCall(URL, new callbackService() {
            @Override
            public void success(VolleyResponseModel callback) {
                try {
                    JSONArray jsonArray = new JSONArray(callback.getResponse());
                    if (jsonArray.length() == 10) {
                        flag = true;
                        page++;
                    } else {
                        flag = false;
                    }
                    for (int i = 0; i < jsonArray.length(); i++) {
                        CategoriesModel commentModel = new CategoriesModel();
                        mData.add(commentModel);
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        if (jsonObject.has("id") && !jsonObject.isNull("id")) {
                            commentModel.setId(jsonObject.getInt("id"));
                        }
                        if (jsonObject.has("name") && !jsonObject.isNull("name")) {
                            commentModel.setName(jsonObject.getString("name"));
                        }
                        if (jsonObject.has("image") && !jsonObject.isNull("image")) {
                            Object image = jsonObject.get("image");
                            if (image instanceof JSONObject) {
                                if (((JSONObject) image).has("src") && !((JSONObject) image).isNull("src")) {
                                    commentModel.setImage(((JSONObject) image).getString("src"));
                                }
                            } else if (image instanceof String) {
                                commentModel.setImage((String) image);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mAdapter.notifyDataSetChanged();
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
            }

            @Override
            public void error(VolleyResponseModel callback) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Utils.Toast("error");
            }
        });
    }
}
