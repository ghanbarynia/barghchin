package com.barghchin.mobile.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.barghchin.mobile.ModelData.NameValuePair;
import com.barghchin.mobile.ModelData.ShoppingBag;
import com.barghchin.mobile.ModelData.UserProfileModel;
import com.barghchin.mobile.ModelData.VolleyResponseModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.utility.MySharedPreferences;
import com.barghchin.mobile.utility.RequestQuery;
import com.barghchin.mobile.utility.RestRequest;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.callbackService;
import com.barghchin.mobile.utility.utilsBasket;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PaymentActivity extends AppCompatActivity implements View.OnClickListener {

    private PaymentActivity mActivity;
    private Spinner mspinner_shipping_method, mspinner_payment_getway;
    private List<NameValuePair> modelShippingMethods = new ArrayList<>(), modelPaymentMethods = new ArrayList<>();
    private MaterialDialog pDialog;
    private UserProfileModel model;
    private TextView mTvTotalSum;
    private int sumTotal = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        initViews();
        loadData();
    }

    private void loadData() {
        sumTotal = utilsBasket.newInstance().getTotalBaskets();
        mTvTotalSum.setText(String.valueOf(sumTotal).concat(" ريال"));

        ShowDialog();
        model = (UserProfileModel) getIntent().getSerializableExtra("UserProfile");
        String URL = new RequestQuery(RequestQuery.UrlOAuthType.GET_SHIPPING_ZONE).addRoute(String.valueOf(getIntent().getIntExtra("idLocation", -1))).addRoute("methods").GetUrl();
        RestRequest.getInstance().AsynchronousServiceCall(URL, new callbackService() {
            @Override
            public void success(VolleyResponseModel callback) {
                try {
                    JSONArray array = new JSONArray(callback.getResponse());
                    for (int i = 0; i < array.length(); i++) {
                        NameValuePair temp_ShippingMethods = new NameValuePair();

                        JSONObject jsonObject = array.getJSONObject(i);

                        if (jsonObject.has("method_id") && !jsonObject.isNull("method_id")) {
                            temp_ShippingMethods.setKey(jsonObject.getString("method_id"));
                        }

                        if (jsonObject.has("settings") && !jsonObject.isNull("settings")) {
                            JSONObject settings = jsonObject.getJSONObject("settings");
                            if (settings.has("cost") && !settings.isNull("cost")) {
                                JSONObject cost = settings.getJSONObject("cost");
                                if (cost.has("value") && !cost.isNull("value")) {
                                    temp_ShippingMethods.setPrice(Integer.parseInt(cost.getString("value")));
                                }
                            }
                        }

                        if (jsonObject.has("title") && !jsonObject.isNull("title")) {
                            temp_ShippingMethods.setValue(jsonObject.getString("title") + (temp_ShippingMethods.getPrice() > 0 ? " (" + temp_ShippingMethods.getPrice() + ")" : ""));
                        }

                        modelShippingMethods.add(temp_ShippingMethods);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mspinner_shipping_method.setAdapter(new ArrayAdapter<>(mActivity, android.R.layout.simple_spinner_dropdown_item, modelShippingMethods));
                DismissDialog();
            }

            @Override
            public void error(VolleyResponseModel callback) {
                onBackPressed();
            }
        });

        String PaymentMethods = MySharedPreferences.getInstance().getString(MySharedPreferences.PaymentMethods, "[]");

        try {
            JSONArray array = new JSONArray(PaymentMethods);
            for (int i = 0; i < array.length(); i++) {
                NameValuePair temp_PaymentMethods = new NameValuePair();

                JSONObject jsonObject = array.getJSONObject(i);

                if (jsonObject.has("id") && !jsonObject.isNull("id")) {
                    temp_PaymentMethods.setKey(jsonObject.getString("id"));
                }

                if (jsonObject.has("title") && !jsonObject.isNull("title")) {
                    temp_PaymentMethods.setValue(jsonObject.getString("title"));
                }
                modelPaymentMethods.add(temp_PaymentMethods);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mspinner_payment_getway.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, modelPaymentMethods));
    }

    private void initViews() {
        mActivity = this;
        findViewById(R.id.Iv_Back).setOnClickListener(this);
        findViewById(R.id.ic_shopping_cart).setOnClickListener(this);
        findViewById(R.id.ic_menu_search).setOnClickListener(this);
        findViewById(R.id.TopToolbar).setBackgroundColor(Utils.getAppThemeColor());
        TextView mTvTitle = (TextView) findViewById(R.id.Tv_Title);
        mTvTitle.setText("پرداخت");

        mspinner_shipping_method = (Spinner) findViewById(R.id.spinner_shipping_method);
        mspinner_payment_getway = (Spinner) findViewById(R.id.spinner_payment_getway);
        mTvTotalSum = (TextView) findViewById(R.id.tv_totalSum);

        mspinner_shipping_method.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                NameValuePair valuePair = (NameValuePair) mspinner_shipping_method.getSelectedItem();
                if (valuePair.getPrice() > 0) {
                    sumTotal = sumTotal + valuePair.getPrice();
                } else {
                    sumTotal = utilsBasket.newInstance().getTotalBaskets();
                }
                mTvTotalSum.setText(String.valueOf(sumTotal).concat(" ريال"));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        findViewById(R.id.pay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, String> par = new HashMap<>();
                par.put("payment_method", ((NameValuePair) mspinner_payment_getway.getSelectedItem()).getKey());
                par.put("payment_method_title", ((NameValuePair) mspinner_payment_getway.getSelectedItem()).getValue());
                par.put("set_paid", "true");
                par.put("billing", new Gson().toJson(model));
                par.put("shipping", new Gson().toJson(model));


                final List<ShoppingBag> bagModel = utilsBasket.newInstance().getBaskets();
                JSONArray jsonArray_items = new JSONArray();
                for (int i = 0; i < bagModel.size(); i++) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("product_id", bagModel.get(i).getId());
                        jsonObject.put("quantity", bagModel.get(i).getCount());
                        if (bagModel.get(i).getAttribute() != null) {
                            jsonObject.put("variation_id", bagModel.get(i).getAttribute().getId());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    jsonArray_items.put(jsonObject);
                }

                par.put("line_items", jsonArray_items.toString());
                JSONArray jsonArray_shipping = new JSONArray();
                JSONObject jsonObject_shipping = new JSONObject();

                try {
                    jsonObject_shipping.put("method_id", ((NameValuePair) mspinner_shipping_method.getSelectedItem()).getKey());
                    jsonObject_shipping.put("method_title", ((NameValuePair) mspinner_shipping_method.getSelectedItem()).getValue());
                    jsonObject_shipping.put("total", 10);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                jsonArray_shipping.put(jsonObject_shipping);
                par.put("shipping_lines", jsonArray_shipping.toString());
                String URL = new RequestQuery(RequestQuery.UrlOAuthType.POST_ORDER).GetUrl();

                ShowDialog();
                RestRequest.getInstance().AsynchronousServiceCall(URL, Request.Method.POST, par, new callbackService() {
                    @Override
                    public void success(VolleyResponseModel callback) {
                        DismissDialog();
                        String order_key = "", id = "";
                        try {
                            JSONObject jsonObject = new JSONObject(callback.getResponse());
                            if (jsonObject.has("order_key") && !jsonObject.isNull("order_key")) {
                                order_key = jsonObject.getString("order_key");
                            }
                            if (jsonObject.has("id") && !jsonObject.isNull("id")) {
                                id = jsonObject.getString("id");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        startActivity(new Intent(mActivity, WebViewActivity.class).putExtra("id", id).putExtra("order_key", order_key));
                        finish();
                    }

                    @Override
                    public void error(VolleyResponseModel callback) {
                        DismissDialog();
                    }
                });
            }
        });

        final EditText mEtCoupon = (EditText) findViewById(R.id.et_coupon);
        findViewById(R.id.btn_add_coupon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(mEtCoupon.getText().toString().trim())) {
                    Utils.Toast("کپن را وارد کنید");
                    return;
                }
                ShowDialog();
                String url = new RequestQuery(RequestQuery.UrlOAuthType.GET_COUPONS).add("code", mEtCoupon.getText().toString()).GetUrl();
                RestRequest.getInstance().AsynchronousServiceCall(url, new callbackService() {
                    @Override
                    public void success(VolleyResponseModel callback) {
                        DismissDialog();
                        try {

                            String response = callback.getResponse();
                            if (TextUtils.isEmpty(response)) {
                                Utils.Toast("کپن نادرست است.");
                                return;
                            }

                            JSONArray jsonArray = new JSONArray(response);

                            if (jsonArray.length() == 0) {
                                Utils.Toast("کپن نادرست است.");
                                return;
                            }

                            JSONObject jsonObject = jsonArray.getJSONObject(0);


                            if (jsonObject == null) {
                                Utils.Toast("کپن نادرست است.");
                                return;
                            }

                            boolean couponValid = true;
                            String message = "";


                            int usage_count = 0;
                            boolean individual_use = false;


                            JSONArray product_categories = null; // array list
                            JSONArray excluded_product_categories = null; // array list

                            //float minimum_amount = 0;
                            //float maximum_amount = 0;


                            do {


                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
                                sdf.setTimeZone(TimeZone.getTimeZone("Asia/Tehran"));
                                long time = sdf.parse(jsonObject.getString("date_expires")).getTime();
                                time = time - System.currentTimeMillis();

                                if (time <= 0) {
                                    couponValid = false;
                                    message = "تاریخ انقضا کپن به اتمام رسیده است.";
                                    break;
                                }



                               /* usage_count = jsonObject.getInt("usage_count");

                                if (usage_count == 0) {
                                    couponValid = false;
                                    message = "تعداد استفاده کپن به اتمام رسیده است.";
                                    break;
                                }*/

                                if (jsonObject.has("free_shipping") && !jsonObject.isNull("free_shipping") && jsonObject.getBoolean("free_shipping")) {
                                    sumTotal = utilsBasket.newInstance().getTotalBaskets();
                                }

                                List<ShoppingBag> baskets = utilsBasket.newInstance().getBaskets();

                                //اگر درست باشد، این کوپن به اقلامی که دارای قیمت حراجی هستند اعمال نخواهد شد

                                boolean has_sale_item = false;
                                if (jsonObject.getBoolean("exclude_sale_items")) {
                                    for (ShoppingBag shoppingBag : baskets) {
                                        if (shoppingBag.isOn_sale()) {
                                            has_sale_item = true;
                                            break;
                                        }
                                    }
                                }
                                if (has_sale_item) {
                                    message = "این کپن شامل محصولات حراجی نمی باشد";
                                    break;
                                }


                                List<Integer> excluded_product_ids = new Gson().fromJson(jsonObject.getString("excluded_product_ids"), new TypeToken<List<Integer>>() {
                                }.getType());
                                boolean has_excluded_product = false;
                                for (ShoppingBag shoppingBag : baskets) {
                                    if (excluded_product_ids.contains(shoppingBag.getId())) {
                                        has_excluded_product = true;
                                        break;
                                    }
                                }

                                if (has_excluded_product) {
                                    message = "محصولات سبد جزء محصولات مجاز کپن نمی باشند.";
                                    break;
                                }


                                List<Integer> product_ids = new Gson().fromJson(jsonObject.getString("product_ids"), new TypeToken<List<Integer>>() {
                                }.getType());
                                boolean has_included_product = false;
                                for (ShoppingBag shoppingBag : baskets) {
                                    if (!product_ids.contains(shoppingBag.getId())) {
                                        has_included_product = true;
                                        break;
                                    }
                                }

                                if (has_included_product) {
                                    message = "محصولات سبد جزء محصولات مجاز کپن نمی باشند.";
                                    break;
                                }


                                List<String> email_restrictions = new Gson().fromJson(jsonObject.getString("email_restrictions"), new TypeToken<List<String>>() {
                                }.getType());

                                if (!email_restrictions.contains(model.getEmail())) {
                                    message = "ایمیل وارد شده مجاز به استفاده از این کپن نمی باشد.";
                                    break;
                                }


                                List<String> used_by_ = new Gson().fromJson(jsonObject.getString("used_by"), new TypeToken<List<String>>() {
                                }.getType());

                                if (!used_by_.contains(String.valueOf(model.getId()))) {
                                    message = "کد کاربری مجاز به استفاده از این کپن نمی باشد.";
                                    break;
                                }


                                String discount_type = jsonObject.getString("discount_type");
                                int amountOrPercent = jsonObject.getInt("amount");
                                if ("percent".equalsIgnoreCase(discount_type)) {
                                    int percent = (sumTotal * amountOrPercent) / 100;
                                    mTvTotalSum.setText(String.valueOf(sumTotal - percent).concat(" ريال"));
                                } else if ("fixed_cart".equalsIgnoreCase(discount_type)) {
                                    mTvTotalSum.setText(String.valueOf(sumTotal - amountOrPercent).concat(" ريال"));
                                } else if ("fixed_product".equalsIgnoreCase(discount_type)) {
                                    for (ShoppingBag shoppingBag : baskets) {
                                        if (product_ids.contains(shoppingBag.getId())) {
                                            sumTotal = sumTotal - amountOrPercent;
                                            mTvTotalSum.setText(String.valueOf(sumTotal).concat(" ريال"));
                                        }
                                    }
                                }


         /*                       product_categories = jsonObject.getJSONArray("product_categories");

                                if (product_categories != null && product_categories.length() > 0) {
                                    //basket items id
                                    if (!isCommon(new String[]{"one", "two", "three"}, product_categories)) {
                                        couponValid = false;
                                        message = "دسته بندی محصولات سبد جزء دسته بندی مجاز کپن نمی باشند.";
                                        break;
                                    }
                                }*/

                              /*  excluded_product_categories = jsonObject.getJSONArray("excluded_product_categories");

                                if (excluded_product_categories != null && excluded_product_categories.length() > 0) {
                                    //basket items id
                                    if (isCommon(new String[]{"one", "two", "three"}, excluded_product_categories)) {
                                        couponValid = false;
                                        message = "دسته بندی محصولات سبد جزء دسته بندی مجاز کپن نمی باشند.";
                                        break;
                                    }
                                }*/

                                break;
                            } while (false);

                            if (!couponValid) {
                                Utils.Toast(message);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void error(VolleyResponseModel callback) {

                        DismissDialog();
                    }
                });
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void ShowDialog() {
        if (pDialog == null) {
            pDialog = Utils.CustomDialog(mActivity).content(R.string.please_wait).progress(true, 0).show();
        } else {
            pDialog.show();
        }
    }


    public boolean isCommon(String[] itemArray, JSONArray jsonArray) {
        ArrayList<String> stringArray = new ArrayList<>();
        for (int i = 0, count = jsonArray.length(); i < count; i++) {
            try {
                JSONObject jsonItem = jsonArray.getJSONObject(i);
                stringArray.add(jsonItem.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        List<String> base = new ArrayList<>(stringArray);
        base.retainAll(Arrays.asList(itemArray));

        return base.size() > 0;
    }

    private void DismissDialog() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ic_shopping_cart:
                Utils.startShoppingBagActivity(mActivity);
                break;
            case R.id.ic_menu_search:
                startActivity(new Intent(mActivity, SearchActivity.class));
                break;
            case R.id.Iv_Back:
                onBackPressed();
                break;
        }
    }
}
