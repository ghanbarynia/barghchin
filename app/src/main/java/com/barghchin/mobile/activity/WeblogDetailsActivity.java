package com.barghchin.mobile.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;

import com.barghchin.mobile.ModelData.WeblogModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.utility.Utils;

public class WeblogDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private WeblogModel modelData;
    private ImageView mImageView_background;
    private WebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weblog_details);

        initView();
        modelData = (WeblogModel) getIntent().getSerializableExtra("MODEL");
        Utils.loadImage(mImageView_background, modelData.getImage());
        mWebView.loadUrl(modelData.getLink());
    }

    private void initView() {
        findViewById(R.id.Iv_Back).setOnClickListener(this);
        mImageView_background = (ImageView) findViewById(R.id.Image_Background);
        mWebView = (WebView) findViewById(R.id.webView);
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.Iv_Back:
                onBackPressed();
                break;
        }
    }
}
