package com.barghchin.mobile.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.barghchin.mobile.Adapter.CustomAdapter_Weblog;
import com.barghchin.mobile.ModelData.VolleyResponseModel;
import com.barghchin.mobile.ModelData.WeblogModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.utility.RequestQuery;
import com.barghchin.mobile.utility.RestRequest;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.callbackService;
import com.barghchin.mobile.utility.mApplication;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class WeblogActivity extends AppCompatActivity implements View.OnClickListener {
    private WeblogActivity mActivity;
    private MaterialDialog pDialog;
    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_category);
        initView();
        LoadData();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ic_shopping_cart:
                Utils.startShoppingBagActivity(mActivity);
                break;
            case R.id.ic_menu_search:
                Utils.startSearchActivity(mActivity);
                break;
            case R.id.Iv_Back:
                onBackPressed();
                break;
        }
    }

    private void LoadData() {
        ShowDialog();
        String URL = new RequestQuery(RequestQuery.UrlOAuthType.GET_POSTS)/*.addRoute("143")*/.GetUrl();
        RestRequest.getInstance().AsynchronousServiceCall(URL, new callbackService() {
            @Override
            public void success(VolleyResponseModel callback) {
                DismissDialog();
                List<WeblogModel> mData = new ArrayList<>();

                try {
                    JSONArray jsonArray = new JSONArray(callback.getResponse());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        WeblogModel weblogModel = new WeblogModel();
                        if (jsonObject.has("id") && !jsonObject.isNull("id")) {
                            weblogModel.setId(jsonObject.getInt("id"));
                        }
                        if (jsonObject.has("date") && !jsonObject.isNull("date")) {
                            weblogModel.setDate(jsonObject.getString("date"));
                        }
                        if (jsonObject.has("link") && !jsonObject.isNull("link")) {
                            weblogModel.setLink(jsonObject.getString("link"));
                        }
                        if (jsonObject.has("title") && !jsonObject.isNull("title") && jsonObject.getJSONObject("title").has("rendered") && !jsonObject.getJSONObject("title").isNull("rendered")) {
                            weblogModel.setTitle(jsonObject.getJSONObject("title").getString("rendered"));
                        }
                        if (jsonObject.has("content") && !jsonObject.isNull("content") && jsonObject.getJSONObject("content").has("rendered") && !jsonObject.getJSONObject("content").isNull("rendered")) {
                            weblogModel.setContent(jsonObject.getJSONObject("content").getString("rendered"));
                        }
                        if (jsonObject.has("persian_woocommerce_featured_image") && !jsonObject.isNull("persian_woocommerce_featured_image") && jsonObject.getJSONObject("persian_woocommerce_featured_image").has("source_url") && !jsonObject.getJSONObject("persian_woocommerce_featured_image").isNull("source_url")) {
                            weblogModel.setImage(jsonObject.getJSONObject("persian_woocommerce_featured_image").getString("source_url"));
                        }
                        mData.add(weblogModel);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                mRecyclerView.setAdapter(new CustomAdapter_Weblog(mActivity, mData));
            }

            @Override
            public void error(VolleyResponseModel callback) {
                DismissDialog();

            }
        });
    }

    private void initView() {
        mActivity = this;
        findViewById(R.id.Iv_Back).setOnClickListener(this);
        findViewById(R.id.ic_shopping_cart).setOnClickListener(this);
        findViewById(R.id.ic_menu_search).setOnClickListener(this);
        TextView mTvTitle = (TextView) findViewById(R.id.Tv_Title);
        findViewById(R.id.TopToolbar).setBackgroundColor(Utils.getAppThemeColor());
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_ProductCategoryActivity);
        mTvTitle.setTypeface(mApplication.IRANSansBold);
        mTvTitle.setText("وبلاگ");
        final LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setAutoMeasureEnabled(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setLayoutManager(layoutManager);
    }

    private void ShowDialog() {
        if (pDialog == null) {
            pDialog = Utils.CustomDialog(mActivity).content(R.string.please_wait).progress(true, 0).show();
        } else {
            pDialog.show();
        }
    }

    private void DismissDialog() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }
}
