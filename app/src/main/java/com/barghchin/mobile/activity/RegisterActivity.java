package com.barghchin.mobile.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.barghchin.mobile.ModelData.UserProfileModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.utility.Constant;
import com.barghchin.mobile.utility.MySharedPreferences;
import com.barghchin.mobile.utility.RequestQuery;
import com.barghchin.mobile.utility.RestRequest;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.mApplication;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mInputName;
    private EditText mInputLastName;
    private EditText mInputPhone;
    private EditText mInputMail;
    private Spinner mSpinnerCountry;
    private Spinner mSpinnerState;
    private EditText mInputCity;
    private EditText mInputAddress1;
    private EditText mInputPostCode;
    private RegisterActivity mActivity;
    private MaterialDialog pDialog;
    private UserProfileModel profileModel;

    private JSONArray ShippingZoneArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initView();
        loadData();
    }

    private void loadData() {
        profileModel = new Gson().fromJson(MySharedPreferences.getInstance().getString(MySharedPreferences.USER_PROFILE, ""), UserProfileModel.class);
        if (profileModel != null && profileModel.getId() > 0) {
            mInputName.setText(profileModel.getFirst_name());
            mInputLastName.setText(profileModel.getLast_name());
            mInputPhone.setText(profileModel.getPhone());
            mInputMail.setText(profileModel.getEmail());
            mInputCity.setText(profileModel.getCity());
            mInputAddress1.setText(profileModel.getAddress_1());
            mInputPostCode.setText(profileModel.getPostcode());
            if (!TextUtils.isEmpty(profileModel.getState())) {
                for (int i = 0; i < Constant.Value_States.length; i++) {
                    if (Constant.Value_States[i].equalsIgnoreCase(profileModel.getState())) {
                        mSpinnerState.setSelection(i);
                        break;
                    }
                }
            }
        }
        getShippingZone();
    }


    private void getShippingZone() {
        ShowDialog();
        new Thread(new Runnable() {
            @Override
            public void run() {
                ShippingZoneArray = new JSONArray();
                try {
                    String URL_ShippingZone = new RequestQuery(RequestQuery.UrlOAuthType.GET_SHIPPING_ZONE).GetUrl();
                    String response_ShippingZone = RestRequest.getInstance().SynchronousServiceCall(URL_ShippingZone, Request.Method.GET, null).getResponse();
                    if (!TextUtils.isEmpty(response_ShippingZone)) {
                        JSONArray tempJSONArray_ShippingZone = new JSONArray(response_ShippingZone);
                        for (int i = 1; i < tempJSONArray_ShippingZone.length(); i++) {
                            if (tempJSONArray_ShippingZone.getJSONObject(i).has("id") && !tempJSONArray_ShippingZone.getJSONObject(i).isNull("id")) {
                                int id = tempJSONArray_ShippingZone.getJSONObject(i).getInt("id");
                                String URL_Locations = new RequestQuery(RequestQuery.UrlOAuthType.GET_SHIPPING_ZONE).addRoute(String.valueOf(id)).addRoute("locations").GetUrl();
                                String response_Locations = RestRequest.getInstance().SynchronousServiceCall(URL_Locations, Request.Method.GET, null).getResponse();
                                if (!TextUtils.isEmpty(response_Locations)) {
                                    String code = "", type = "";
                                    JSONArray tempJSONArray_Locations = new JSONArray(response_Locations);
                                    JSONObject tempJsonObject_Locations = tempJSONArray_Locations.getJSONObject(0);
                                    if (tempJsonObject_Locations.has("code") && !tempJsonObject_Locations.isNull("code")) {
                                        code = tempJsonObject_Locations.getString("code");
                                    }
                                    if (tempJsonObject_Locations.has("type") && !tempJsonObject_Locations.isNull("type")) {
                                        type = tempJsonObject_Locations.getString("type");
                                    }
                                    JSONObject ShippingZoneObject = new JSONObject();
                                    ShippingZoneObject.put("id", id);
                                    ShippingZoneObject.put("code", code);
                                    ShippingZoneObject.put("type", type);
                                    ShippingZoneArray.put(ShippingZoneObject);
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mApplication.applicationHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        DismissDialog();
                    }
                });
            }
        }).start();
    }

    private void initView() {

        mActivity = this;
        findViewById(R.id.TopToolbar).setBackgroundColor(Utils.getAppThemeColor());
        TextView mTvTitle = (TextView) findViewById(R.id.Tv_Title);
        mTvTitle.setText(getString(R.string.Creating_account));

        mInputName = (EditText) findViewById(R.id.input_name);
        mInputLastName = (EditText) findViewById(R.id.input_last_name);
        mInputPhone = (EditText) findViewById(R.id.input_phone);
        mInputMail = (EditText) findViewById(R.id.input_mail);
        mSpinnerCountry = (Spinner) findViewById(R.id.spinner_country);
        mSpinnerState = (Spinner) findViewById(R.id.spinner_state);
        mInputCity = (EditText) findViewById(R.id.input_city);
        mInputAddress1 = (EditText) findViewById(R.id.input_address1);
        mInputPostCode = (EditText) findViewById(R.id.input_post_code);

        mSpinnerCountry.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, Constant.Value_Countries));
        mSpinnerState.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, Constant.Value_States));

        findViewById(R.id.Iv_Back).setOnClickListener(this);
        findViewById(R.id.ic_shopping_cart).setOnClickListener(this);
        findViewById(R.id.ic_menu_search).setOnClickListener(this);
        findViewById(R.id.btn_save_info).setOnClickListener(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_save_info:
                if (TextUtils.isEmpty(mInputName.getText().toString())) {
                    mInputName.setError("لطفا نام را وارد کنید.");
                    mInputName.requestFocus();
                    break;
                } else {
                    mInputName.setError(null);
                }
                if (TextUtils.isEmpty(mInputLastName.getText().toString())) {
                    mInputLastName.setError("لطفا نام خانوادگی را وارد کنید.");
                    mInputLastName.requestFocus();
                    break;
                } else {
                    mInputLastName.setError(null);
                }
                if (TextUtils.isEmpty(mInputPhone.getText().toString())) {
                    mInputPhone.setError("لطفا موبایل را وارد کنید.");
                    mInputPhone.requestFocus();
                    break;
                } else {
                    mInputPhone.setError(null);
                }
                if (TextUtils.isEmpty(mInputMail.getText().toString())) {
                    mInputMail.setError("لطفا ایمیل را وارد کنید.");
                    mInputMail.requestFocus();
                    break;
                } else {
                    mInputMail.setError(null);
                }
                if (TextUtils.isEmpty(mInputCity.getText().toString())) {
                    mInputCity.setError("لطفا شهر را وارد کنید.");
                    mInputCity.requestFocus();
                    break;
                } else {
                    mInputCity.setError(null);
                }
                if (TextUtils.isEmpty(mInputAddress1.getText().toString())) {
                    mInputAddress1.setError("لطفا آدرس را وارد کنید.");
                    mInputAddress1.requestFocus();
                    break;
                } else {
                    mInputAddress1.setError(null);
                }
                if (TextUtils.isEmpty(mInputPostCode.getText().toString())) {
                    mInputPostCode.setError("لطفا کد پستی را وارد کنید.");
                    mInputPostCode.requestFocus();
                    break;
                } else {
                    mInputPostCode.setError(null);
                }

                int idCountryIR = 0;
                int idLocation = 0;

                if (ShippingZoneArray == null || ShippingZoneArray.length() == 0) {
                    Utils.Toast("هیچ روش حمل و نقل برای فروشگاه تعریف نشده.");
                    return;
                }

                for (int i = 0; i < ShippingZoneArray.length(); i++) {
                    try {
                        JSONObject obj = ShippingZoneArray.getJSONObject(i);

                        String code = "";
                        String type = "";
                        int id = 0;

                        if (!obj.isNull("code") && obj.has("code")) {
                            code = obj.getString("code");
                        }

                        if (!obj.isNull("type") && obj.has("type")) {
                            type = obj.getString("type");
                        }

                        if (!obj.isNull("id") && obj.has("id")) {
                            id = obj.getInt("id");
                        }

                        if (code.equalsIgnoreCase("IR") && type.equalsIgnoreCase("country")) {
                            idCountryIR = id;
                        }


                        if (code.indexOf(Constant.Key_States[mSpinnerState.getSelectedItemPosition()]) > -1) {
                            idLocation = id;
                            break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                if (idCountryIR == 0 && idLocation == 0) {
                    Utils.Toast("هیچ روش حمل و نقل برای استان انتخابی پیدا نشد");
                    return;
                }

                final UserProfileModel model = new UserProfileModel();
                model.setId(profileModel.getId());
                model.setFirst_name(mInputName.getText().toString());
                model.setLast_name(mInputLastName.getText().toString());
                model.setAddress_1(mInputAddress1.getText().toString());
                model.setCity(mInputCity.getText().toString());
                model.setState("RKH"/*Constant.Key_States[mSpinnerState.getSelectedItemPosition()]*/);
                model.setPostcode(mInputPostCode.getText().toString());
                model.setCountry("IR");
                model.setEmail(mInputMail.getText().toString());
                model.setPhone(mInputPhone.getText().toString());

                startActivity(new Intent(mActivity, PaymentActivity.class).putExtra("UserProfile", model).putExtra("idLocation", idLocation == 0 ? idCountryIR : idLocation));
                finish();

                break;

            case R.id.ic_shopping_cart:
                Utils.startShoppingBagActivity(mActivity);
                break;

            case R.id.ic_menu_search:
                Utils.startSearchActivity(mActivity);
                break;

            case R.id.Iv_Back:
                onBackPressed();
                break;
        }
    }

    private void ShowDialog() {
        if (pDialog == null) {
            pDialog = Utils.CustomDialog(mActivity).content(R.string.please_wait).progress(true, 0).show();
        } else {
            pDialog.show();
        }
    }

    private void DismissDialog() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }
}
