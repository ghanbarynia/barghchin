package com.barghchin.mobile.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.barghchin.mobile.Adapter.ShoppingBagAdapter;
import com.barghchin.mobile.ModelData.ShoppingBag;
import com.barghchin.mobile.ModelData.ShoppingBagModel;
import com.barghchin.mobile.R;
import com.barghchin.mobile.utility.BackGroundJobs;
import com.barghchin.mobile.utility.MySharedPreferences;
import com.barghchin.mobile.utility.Utils;
import com.barghchin.mobile.utility.mApplication;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ShoppingBagActivity extends AppCompatActivity {

    private ListView mListView;
    private MaterialDialog pDialog;
    private ShoppingBagActivity mActivity;
    private TextView mTvTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_bag);
        initView();
        LoadData();
    }

    private void LoadData() {
        String array = MySharedPreferences.getInstance().getString(MySharedPreferences.SHOPPING_BAG, "");
        final ShoppingBagModel bagModel = ShoppingBagModel.fromString(array);
        mListView.setAdapter(new ShoppingBagAdapter(mActivity, bagModel.getBags()));
        mListView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {

            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        MySharedPreferences.getInstance().setPreference(MySharedPreferences.SHOPPING_BAG, bagModel.toString());
                        int Total = 0;
                        for (ShoppingBag bag : bagModel.getBags()) {
                            Total += Integer.parseInt(bag.getPrice()) * bag.getCount();
                        }
                        final int finalTotal = Total;
                        mApplication.applicationHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                mTvTotal.setText(String.valueOf(finalTotal).concat(" ريال"));
                            }
                        });
                    }
                }).start();
            }
        });


    }

    private void initView() {
        mActivity = this;
        findViewById(R.id.TopToolbar).setBackgroundColor(Utils.getAppThemeColor());
        TextView mTvTitle = (TextView) findViewById(R.id.Tv_Title);
        mTvTitle.setText("سبد خرید");
        mTvTotal = (TextView) findViewById(R.id.tv_total_card);
        mListView = (ListView) findViewById(R.id.listView_ShoppingBagActivity);
        findViewById(R.id.Iv_Back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        findViewById(R.id.ic_menu_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mActivity, SearchActivity.class));
            }
        });
        findViewById(R.id.ic_shopping_cart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.startShoppingBagActivity(mActivity);
            }
        });
        findViewById(R.id.Finalize_purchase).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backgroundJobs();
                startActivity(new Intent(mActivity, RegisterActivity.class));
                finish();
            }
        });
    }

    private void backgroundJobs() {
        BackGroundJobs.webservicePaymentMethods();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void showDialog() {
        if (pDialog == null) {
            pDialog = Utils.CustomDialog(mActivity).content(R.string.please_wait).progress(true, 0).show();
        } else {
            pDialog.show();
        }
    }

    private void showDismiss() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }
}
